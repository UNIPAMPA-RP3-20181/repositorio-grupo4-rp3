SELECT tipoparentesco.descricao as parentesco, pessoas.id, pessoas.nome, pessoas.cpf, pessoas.email, pessoas.telefone, pessoas.datanascimento, pessoas.sexo, pessoas.cep, pessoas.cidade, pessoas.endereco, pessoas.bairro
FROM pessoas, beneficiarios, tipoparentesco
WHERE pessoas.id = 1 
    AND beneficiarios.idcandidato = pessoas.id
    AND beneficiarios.parentesco = tipoparentesco.id;