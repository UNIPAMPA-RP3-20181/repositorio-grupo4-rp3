/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Date;

/**
 *
 * @author mathias
 */
public class MockCandidato extends Candidato {

    public MockCandidato() {
        super("matheusalzenberg",
                "umasenhadificil",
                "Matheus Alzenberg",
                141223050, //cpf
                "matheusalzenberg@gmail.com",
                992072304,
                new Date(1999, 4, 3), //nasc
                'M', //sexo
                98700000, //cep
                "Ijui",//cidade
                "Rua dos Dragões Negros", //endereco
                "São Jorge");//bairro
        super.addBeneficiario(new Beneficiario(Parentesco.FILHO,
                "Matheus Alzenberg Junior",
                456784566, //cpf
                "matheusalzenbergjunior@gmail.com",
                991483714,
                new Date(2015, 3, 4), //nasc
                'M', //sexo
                98700000, //cep
                "Ijui",//cidade
                "Rua dos Dragões Negros", //endereco
                "São Jorge"));
    }
}