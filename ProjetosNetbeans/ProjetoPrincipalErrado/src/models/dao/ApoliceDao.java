/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import models.Model;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class ApoliceDao extends DAO {

    public ApoliceDao() {
    }

    @Override
    public void delete(Model model) {

    }

    @Override
    public void update(Model model) {

    }

    @Override
    public void insert(Model model) throws SQLException, IllegalStateException, IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Model> select() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String consultarPorNumero(long numero) throws SQLException {
        String resultado = null;
        conexao.abrirConexao();
//        Statement stmt = conexao.prepararDeclaracao();
//        
//        ResultSet result = stmt.executeQuery("SELECT pessoas.nome "
//                + "FROM apolices, segurados, pessoas "
//                + "WHERE apolices.numero = ? "
//                + "AND segurados.idApolice = apolices.idApolice "
//                + "AND segurados.idUsuario = pessoas.idUsuario");
//
//        resultado = result.getString(1);
//        stmt.close();

        String query = "SELECT pessoas.nome "
                + "FROM apolices, segurados, pessoas "
                + "WHERE apolices.numero = ? "
                + "AND segurados.idApolice = apolices.idApolice "
                + "AND segurados.idUsuario = pessoas.idUsuario";

        PreparedStatement pstmt = conexao.prepararDeclaracao(query);
        pstmt.setLong(1, numero);
        ResultSet rs = pstmt.executeQuery();//then execute the statement
        if (rs != null && rs.next()) {
            resultado = rs.getString("nome");
        }
        
        pstmt.close();

        return resultado;
    }


}
