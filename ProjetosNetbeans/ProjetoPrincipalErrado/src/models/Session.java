package models;

import bancoDeDados.Conexao;
import exceptions.NotFoundOnDBException;
import models.factories.AvaliadorFactory;
import models.factories.CandidatoFactory;
import models.factories.CorretorFactory;

import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class Session{

    private static Session ME;
    private Pessoa pessoaLogada;
    protected Conexao conexao;
    private final String caminho = "./sessionstore/sessionToken.bin";


    private final String salt = UUID.randomUUID().toString();
    private final String tokenizatorSalt = "%&UYHUJ(*#MOS*&&)%NB&$%&$#&@#%*(!*&TG&F&F!##&!*$!#";

    public static Session getInstance() {
        if (ME == null) {
            ME = new Session();
        }
        return ME;
    }

    private Session() {

        this.conexao = Conexao.getInstance();
        conexao.init("localhost", 3306, "segurodevida2", "root", "");
    }

    private void setPessoa(Pessoa p) {
        cleanSessionAndDeleteCache();
        this.pessoaLogada = p;

    }

    public Pessoa getPessoa() {
        return pessoaLogada;
    }

    public String getTipoPessoa() {
        if (pessoaLogada instanceof Candidato)
            return "candidato";
        if (pessoaLogada instanceof Corretor)
            return "corretor";
        if (pessoaLogada instanceof Avaliador)
            return "avaliador";
        else
            return "nenhum";
    }

    public void cleanSession() {
        pessoaLogada = null;
    }

    public void cleanSessionAndDeleteCache() {
        pessoaLogada = null;
        System.out.println("deleted: "+new File(caminho).delete());
    }


    public void saveSession() throws SQLException {
        conexao.abrirConexao();
        int id = this.getPessoa().getId();
        String token = String.valueOf(id) + System.identityHashCode(getPessoa());

        System.out.println("token: "+token);
        String savedToken = Crypt.sha512(token, salt);
        System.out.println("savedToken: "+savedToken);

        String tokenizedToken = Crypt.sha512(savedToken, tokenizatorSalt);
        System.out.println("tokenizedToken: "+tokenizedToken);

        PreparedStatement stmt = conexao.prepararDeclaracao("update usuarios set acesstoken=? where idpessoa=?");
        stmt.setString(1, tokenizedToken);
        stmt.setInt(2, pessoaLogada.getId());
        stmt.executeUpdate();

        try {
            SessionToBeSaved sessionToBeSaved = new SessionToBeSaved(savedToken, id);
            save(sessionToBeSaved);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        conexao.fecharConexao();

    }

    private boolean loadSession() {
        SessionToBeSaved stbs = load();

        if(stbs==null)
            return false;

        int id = stbs.getSavedId();
        String savedToken = stbs.getSavedToken();

        try {
            conexao.abrirConexao();
            PreparedStatement stmt = conexao.prepararDeclaracao("select tipopessoa.descricao as tipopessoa " +
                    "from usuarios, pessoas, tipopessoa " +
                    "where pessoas.tipopessoa=tipopessoa.id " +
                    "and usuarios.idpessoa=pessoas.id " +
                    "and acessToken=? " +
                    "and idpessoa=?");

            stmt.setString(1, Crypt.sha512(savedToken, tokenizatorSalt));
            stmt.setInt(2, id);

            ResultSet rs = stmt.executeQuery();

            if(rs.first()) {
                String tipopessoa = rs.getString("tipopessoa");

                return makePessoaSession(id, tipopessoa);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean makePessoaSession(int id, String tipopessoa) throws SQLException {
        switch (tipopessoa) {
            case "Candidato":
                this.makeCandidatoSession(id);
                return true;
            case "Funcionario":
                PreparedStatement stmt2 = conexao.prepararDeclaracao("select descricao " +
                        "from funcionarios, tipofuncionario " +
                        "where funcionarios.tipofuncionario=tipofuncionario.id " +
                        "and funcionarios.idpessoa=?");
                stmt2.setInt(1, id);
                stmt2.executeQuery();
                stmt2.getResultSet().first();
                switch (stmt2.getResultSet().getString("descricao")) {
                    case "Avaliador":
                        this.makeAvaliadorSession(id);
                        return true;
                    case "Corretor":
                        this.makeCorretorSession(id);
                        return true;
                }
                break;
        }
        return false;
    }

    private SessionToBeSaved load() {
        File f = new File(caminho);
        if (f.exists()) {
            try {
                FileInputStream fis = new FileInputStream(f);
                ObjectInputStream ois = new ObjectInputStream(fis);
                Object lista = ois.readObject();

                ois.close();
                fis.close();
                if (lista != null) {
                    if (lista instanceof SessionToBeSaved)
                        return (SessionToBeSaved) lista;
                }
            } catch (IOException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        }
        return null;

    }

    private void save(SessionToBeSaved stbs) {
        FileOutputStream fout = null;
        ObjectOutputStream oos = null;
        try {
            File f = new File(caminho);
            System.out.println(f.getAbsolutePath());
            f.getParentFile().mkdirs();
            if (f.exists()) {
                if (f.delete())
                    System.out.println("deleted");
                f.createNewFile();

            }
            fout = new FileOutputStream(f);
            oos = new ObjectOutputStream(fout);
            oos.writeObject(stbs);
            System.out.println("Done");

        } catch (Exception ex) {

            ex.printStackTrace();

        } finally {

            if (fout != null) {
                try {
                    fout.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }




    public boolean verifyIfExistArchivedSession() {
        return loadSession();
    }

    public void makeAvaliadorSession(int id) {
        try {
            this.setPessoa(AvaliadorFactory.getAvaliadorById(id));
        } catch (NotFoundOnDBException e) {
            e.printStackTrace();
        }

    }

    public void makeCorretorSession(int id) {
        try {
            this.setPessoa(CorretorFactory.getCorretorById(id));
        } catch (NotFoundOnDBException e) {
            e.printStackTrace();
        }

    }

    public void makeCandidatoSession(int id) {
        try {
            this.setPessoa(CandidatoFactory.getCandidatoById(id));
        } catch (NotFoundOnDBException e) {
            e.printStackTrace();
        }
    }

}