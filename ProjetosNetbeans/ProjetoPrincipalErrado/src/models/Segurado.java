/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Date;

/**
 *
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class Segurado extends Candidato {

    private boolean vivo;
    private boolean incapacitado;
    
    private ApoliceSeguro apoliceSeguro;

    public Segurado(String senha, String nome, String cpf, String email, String telefone, Date dataNascimento, char sexo, String cep, String cidade, String endereco, String bairro, ApoliceSeguro apoliceSeguro) {
        super(nome, cpf, email, telefone, dataNascimento, sexo, cep, cidade, endereco, bairro);
        this.apoliceSeguro = apoliceSeguro;
        System.out.println(vivo);
        System.out.println(incapacitado);
        
    }

    public ApoliceSeguro getApoliceSeguro() {
        return apoliceSeguro;
    }

    public void setApoliceSeguro(ApoliceSeguro apoliceSeguro) {
        this.apoliceSeguro = apoliceSeguro;
    }

    public boolean isVivo() {
        return vivo;
    }

    public void setVivo(boolean vivo) {
        this.vivo = vivo;
    }

    public boolean isIncapacitado() {
        return incapacitado;
    }

    public void setIncapacitado(boolean incapacitado) {
        this.incapacitado = incapacitado;
    }

}
