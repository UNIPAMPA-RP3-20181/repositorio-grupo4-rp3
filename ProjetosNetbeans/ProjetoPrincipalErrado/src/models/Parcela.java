/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Date;

/**
 *
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class Parcela {

    private double valorParcela;
    private Date dataPgtoParcela;

    public Parcela(double valorParcela, Date dataPgtoParcela) {
        this.valorParcela = valorParcela;
        this.dataPgtoParcela = dataPgtoParcela;
    }

    public double getValorParcela() {
        return valorParcela;
    }

    public void setValorParcela(double valorParcela) {
        this.valorParcela = valorParcela;
    }

    public Date getDataPgtoParcela() {
        return dataPgtoParcela;
    }

    public void setDataPgtoParcela(Date dataPgtoParcela) {
        this.dataPgtoParcela = dataPgtoParcela;
    }

}
