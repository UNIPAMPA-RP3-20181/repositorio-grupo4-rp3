package models;

import java.text.Normalizer;

public enum Parentesco {
    PAI("Pai"),
    MAE("Mãe"),
    FILHO("Filho"),
    FILHA("Filha"),
    IRMAO("Irmão"),
    IRMA("Irmã"),
    AVO("Avô"),
    AVÓ("Avó"),
    NETO("Neto"),
    NETA("Neta"),
    TIO("Tio"),
    TIA("Tia"),
    SOBRINHO("Sobrinho"),
    SOBRINHA("Sobrinha"),
    PRIMO("Primo"),
    PRIMA("Prima");

    public static Parentesco getParentesco(String s) {
        s = s.toLowerCase();
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[^\\p{ASCII}]", "");
        switch (s) {
            case "pai":
                return PAI;
            case "mae":
                return MAE;
            case "filho":
                return FILHO;
            case "filha":
                return FILHA;
            case "irmao":
                return IRMAO;
            case "irma":
                return IRMA;
            case "avo":
                return AVO;
            case "avó":
                return AVÓ;
            case "neto":
                return NETO;
            case "neta":
                return NETA;
            case "tio":
                return TIO;
            case "tia":
                return TIA;
            case "sobrinho":
                return SOBRINHO;
            case "sobrinha":
                return SOBRINHA;
            case "primo":
                return PRIMO;
            case "prima":
                return PRIMA;
            default:
                return null;
        }
    }

    private String nome;

    public String getNome() {
        return this.nome;
    }

    private Parentesco(String nome) {
        this.nome = nome;
    }

}
