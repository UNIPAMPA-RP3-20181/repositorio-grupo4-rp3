package models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SolicitacaoSeguro extends Model {

    private int id;

    private int numeroSolicitacao;

    private Date dataSolicitacao;

    private double valorSolicitacao;

    private double valorPremio;

    private Date dataVisitaCandidato;

    private boolean aprovadaSolicitacao;

    private String motivoReprovacao;

    private String alteracoes;

    private Date dataAlteracao;

    private double valorCorrigidoSolicitacao;

    private boolean fumante;

    private boolean alcoolista;

    private List<Predisposicoes> predisposicoes;

    private List<ProblemaSaude> problemasSaude;

    private Candidato candidato;



    public SolicitacaoSeguro(Candidato candidato){
        this.candidato=candidato;
    }

    public void setMultiple(int nSol, Date dataSolicitacao, double valorSolicitacao, double valorPremio, boolean fumante, boolean alcoolista, List<Predisposicoes> predisposicoes){
        this.setNumeroSolicitacao(nSol);
        this.setDataSolicitacao(dataSolicitacao);
        this.setValorSolicitacao(valorSolicitacao);
        this.setValorPremio(valorPremio);
        this.setFumante(fumante);
        this.setAlcoolista(alcoolista);
        this.setPredisposicoes(predisposicoes);
        this.setProblemasSaude(new ArrayList<>());
    }
    public SolicitacaoSeguro(int nSol, Date dataSolicitacao, double valorSolicitacao, double valorPremio, boolean fumante, boolean alcoolista, List<Predisposicoes> predisposicoes, Candidato c) {

        this.setNumeroSolicitacao(nSol);
        this.setDataSolicitacao(dataSolicitacao);
        this.setValorSolicitacao(valorSolicitacao);
        this.setValorPremio(valorPremio);
        this.setFumante(fumante);
        this.setAlcoolista(alcoolista);
        this.setPredisposicoes(predisposicoes);
        this.setProblemasSaude(new ArrayList<>());
        this.setCandidato(c);
    }

    public void adicionarProblema(ProblemaSaude p) {
        getProblemasSaude().add(p);
    }

    public static int gerarNumeroSolicitacao(int dayCode) {
        Calendar c = Calendar.getInstance();
        int ano = c.get(Calendar.YEAR);
        int mes = c.get(Calendar.MONTH);
        int dia = c.get(Calendar.DAY_OF_MONTH);
        return Integer.valueOf("" + ano + "" + mes + "" + dia + "" + dayCode);
    }


    public String getNomeCandidato() {
        return getCandidato().getNome();
    }

    public Candidato getCandidato() {
        return candidato;

    }

    public int getNumeroSolicitacao() {
        return numeroSolicitacao;
    }

    public Date getDataSolicitacao() {
        return dataSolicitacao;
    }

    public double getValorSolicitacao() {
        return valorSolicitacao;
    }

    public Date getDataVisitaCandidato() {
        return dataVisitaCandidato;
    }

    @Override
    public String toString() {
        return "SolicitacaoSeguro{" +
                "numeroSolicitacao=" + getNumeroSolicitacao() +
                ", dataSolicitacao=" + getDataSolicitacao() +
                ", valorSolicitacao=" + getValorSolicitacao() +
                ", dataVisitaCandidato=" + getDataVisitaCandidato() +
                ", aprovadaSolicitacao=" + isAprovadaSolicitacao() +
                ", motivoReprovacao='" + getMotivoReprovacao() + '\'' +
                ", alteracoes='" + getAlteracoes() + '\'' +
                ", valorCorrigidoSolicitacao=" + getValorCorrigidoSolicitacao() +
                ", dataAlteracao=" + getDataAlteracao() +
                ", fumante=" + isFumante() +
                ", alcoolista=" + isAlcoolista() +
                ", predisposicoes=" + getPredisposicoes() +
                ", problemasSaude=" + getProblemasSaude().toString() +
                '}';
    }

    public boolean isAprovadaSolicitacao() {
        return aprovadaSolicitacao;
    }

    public String getMotivoReprovacao() {
        return motivoReprovacao;
    }

    public String getAlteracoes() {
        return alteracoes;
    }

    public double getValorCorrigidoSolicitacao() {
        return valorCorrigidoSolicitacao;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

    public boolean isFumante() {
        return fumante;
    }

    public boolean isAlcoolista() {
        return alcoolista;
    }

    public List<Predisposicoes> getPredisposicoes() {
        return predisposicoes;
    }

    public List<ProblemaSaude> getProblemasSaude() {
        return problemasSaude;
    }

    public void setMotivoReprovacao(String motivoReprovacao) {
        this.motivoReprovacao = motivoReprovacao;
    }

    public void setAlteracoes(String alteracoes) {
        this.alteracoes = alteracoes;
    }

    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    public void setAprovadaSolicitacao(boolean aprovadaSolicitacao) {
        this.aprovadaSolicitacao = aprovadaSolicitacao;
    }

    public void setDataVisitaCandidato(Date dataVisitaCandidato) {
        this.dataVisitaCandidato = dataVisitaCandidato;
    }

    public void setDataVisitaCandidato(int dia, int mes, int ano, int hora, int minuto) {
        if (getDataVisitaCandidato() == null) {
            setDataVisitaCandidato(new Date());
        }
        getDataVisitaCandidato().setDate(dia);
        getDataVisitaCandidato().setMonth(mes);
        getDataVisitaCandidato().setYear(ano);
        getDataVisitaCandidato().setHours(hora);
        getDataVisitaCandidato().setMinutes(minuto);
    }

    public double getValorPremio() {
        return valorPremio;
    }


    public void reprovar(String motivo) {
        this.setAprovadaSolicitacao(false);
        this.setMotivoReprovacao(motivo);
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }


    public void setNumeroSolicitacao(int numeroSolicitacao) {
        this.numeroSolicitacao = numeroSolicitacao;
    }

    public void setDataSolicitacao(Date dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }

    public void setValorSolicitacao(double valorSolicitacao) {
        this.valorSolicitacao = valorSolicitacao;
    }

    public void setValorPremio(double valorPremio) {
        this.valorPremio = valorPremio;
    }

    public void setValorCorrigidoSolicitacao(double valorCorrigidoSolicitacao) {
        this.valorCorrigidoSolicitacao = valorCorrigidoSolicitacao;
    }

    public void setFumante(boolean fumante) {
        this.fumante = fumante;
    }

    public void setAlcoolista(boolean alcoolista) {
        this.alcoolista = alcoolista;
    }

    public void setPredisposicoes(List<Predisposicoes> predisposicoes) {
        this.predisposicoes = predisposicoes;
    }

    public void setProblemasSaude(List<ProblemaSaude> problemasSaude) {
        this.problemasSaude = problemasSaude;
    }

    public void setCandidato(Candidato candidato) {
        this.candidato = candidato;
    }


    public int calcularIdade() {
        Date dataNascimento = candidato.getDataNascimento();
        System.out.println(dataNascimento.toString());
        Date dataAtual = new Date();
        System.out.println(dataAtual.toString());
        int dataAtualI = dataAtual.getYear();
        int dataNascimentoI = dataNascimento.getYear();
        int idade = dataAtualI - dataNascimentoI - 1;
        if (dataNascimento.getMonth() <= dataAtual.getMonth() + 1
                && dataNascimento.getDate() <= dataAtual.getDate()) {
            idade++;
        }
        return idade;
    }

    public float calcularValorPremio(double valorDesejado, int qntddPredisposicoes, boolean isFumante, boolean isAlcoolista) {
        int idade = calcularIdade();
        float valorPremio = 0;
        if (idade >= 18 && idade <= 25) {
            if (candidato.getSexo() == 'M') {
                valorPremio = 617.00f;
            } else {
                valorPremio = 535.50f;
            }
        } else if (idade >= 26 && idade <= 35) {
            if (candidato.getSexo() == 'M') {
                valorPremio = 585.80f;
            } else {
                valorPremio = 516.20f;
            }
        } else if (idade >= 36 && idade <= 45) {
            if (candidato.getSexo() == 'M') {
                valorPremio = 516.95f;
            } else {
                valorPremio = 578.95f;
            }
        } else if (idade >= 46 && idade <= 50) {
            if (candidato.getSexo() == 'M') {
                valorPremio = 557.00f;
            } else {
                valorPremio = 482.17f;
            }
        } else if (idade >= 51 && idade <= 55) {
            if (candidato.getSexo() == 'M') {
                valorPremio = 589.94f;
            } else {
                valorPremio = 488.90f;
            }
        } else if (idade >= 56 && idade <= 60) {
            if (candidato.getSexo() == 'M') {
                valorPremio = 624.90f;
            } else {
                valorPremio = 524.30f;
            }
        } else if (idade >= 61 && idade <= 65) {
            if (candidato.getSexo() == 'M') {
                valorPremio = 656.66f;
            } else {
                valorPremio = 544.41f;
            }
        } else if (idade >= 66 && idade <= 75) {
            if (candidato.getSexo() == 'M') {
                valorPremio = 687.00f;
            } else {
                valorPremio = 581.50f;
            }
        } else if (idade >= 76 && idade <= 85) {
            if (candidato.getSexo() == 'M') {
                valorPremio = 714.32f;
            } else {
                valorPremio = 644.12f;
            }
        } else if (idade >= 86 && idade <= 90) {
            if (candidato.getSexo() == 'M') {
                valorPremio = 851.60f;
            } else {
                valorPremio = 792.70f;
            }
        }

        double valorAcrecimadoPeloValorDesejado = 0;
        if (valorDesejado > 150000) {
            valorDesejado -= 150000;
            valorAcrecimadoPeloValorDesejado = (Math.ceil(valorDesejado / 25000) * (valorPremio * 0.1));
            System.out.println("valor base: " + valorDesejado / 25000 + ", fator multiplicador: " + Math.ceil(valorDesejado / 25000));
        }
        valorPremio += valorAcrecimadoPeloValorDesejado;
        valorPremio += qntddPredisposicoes * 31.64f;
        if (isFumante) {
            valorPremio += 296.36;
        }
        if (isAlcoolista) {
            valorPremio += 203.09;
        }

        return valorPremio;
    }



    public String getValorSolicitacaoString() {
        return String.format("R$%#.2f", valorSolicitacao);
    }

    public String getDataSolicitacaoString() {
        int ano = dataSolicitacao.getYear()+1900;
        int mes= dataSolicitacao.getMonth()+1;
        int dia = dataSolicitacao.getDate();
        return dia+"/"+mes+"/"+ano;
    }

    public String getValorPremioString() {
        return String.format("R$%#.2f", valorPremio);
    }
}

