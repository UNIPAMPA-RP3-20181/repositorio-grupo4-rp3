package models.mocks;

import models.Predisposicoes;
import models.ProblemaSaude;
import models.SolicitacaoSeguro;

import java.util.ArrayList;
import java.util.Date;

public class MockSolicitacaoSeguro2 extends SolicitacaoSeguro {


    public MockSolicitacaoSeguro2() {
        super(gerarNumeroSolicitacao(02),
                new Date(2018, 3, 5), 100000, 687, false, true,
                new ArrayList<>(),
                new MockCandidato());
        super.getPredisposicoes().add(Predisposicoes.DIABETE);
        super.adicionarProblema(new ProblemaSaude("Gengivite Galopante",
                "Essa é a doença que matou o tiozinho de todo mundo odeia o chris"));

    }
}
