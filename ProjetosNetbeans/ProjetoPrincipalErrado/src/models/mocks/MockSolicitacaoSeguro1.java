package models.mocks;

import models.Predisposicoes;
import models.ProblemaSaude;
import models.SolicitacaoSeguro;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MockSolicitacaoSeguro1 extends SolicitacaoSeguro {


    public MockSolicitacaoSeguro1() {
        super(SolicitacaoSeguro.gerarNumeroSolicitacao(01),
                new Date(2018, 3, 2),
                150000, 617, true, false, new ArrayList<>()
                , new MockCandidato());
        super.getPredisposicoes().add(Predisposicoes.DEMENCIA);
        super.adicionarProblema(new ProblemaSaude("Probleminha", "Quando ele joga, tenho probleminha"));

    }
}
