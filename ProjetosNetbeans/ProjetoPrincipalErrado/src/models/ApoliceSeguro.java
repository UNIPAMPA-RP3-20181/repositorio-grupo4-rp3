/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Date;

/**
 *
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class ApoliceSeguro {

    private long numeroApolice;
    private Date dataContratcaoApolice;
    private double premioApolice;
    private double valorSinistroMorte;
    private double valorSinistoIncapacitado;
    
    private boolean ativo;

    public ApoliceSeguro(long numeroApolice, Date dataContratcaoApolice, double premioApolice, double valorSinistroMorte, double valorSinistoIncapacitado) {
        this.numeroApolice = numeroApolice;
        this.dataContratcaoApolice = dataContratcaoApolice;
        this.premioApolice = premioApolice;
        this.valorSinistroMorte = valorSinistroMorte;
        this.valorSinistoIncapacitado = valorSinistoIncapacitado;
        this.ativo = true;
    }

    public long getNumeroApolice() {
        return numeroApolice;
    }

    public void setNumeroApolice(long numeroApolice) {
        this.numeroApolice = numeroApolice;
    }

    public Date getDataContratcaoApolice() {
        return dataContratcaoApolice;
    }

    public void setDataContratcaoApolice(Date dataContratcaoApolice) {
        this.dataContratcaoApolice = dataContratcaoApolice;
    }

    public double getPremioApolice() {
        return premioApolice;
    }

    public void setPremioApolice(double premioApolice) {
        this.premioApolice = premioApolice;
    }

    public double getValorSinistroMorte() {
        return valorSinistroMorte;
    }

    public void setValorSinistroMorte(double valorSinistroMorte) {
        this.valorSinistroMorte = valorSinistroMorte;
    }

    public double getValorSinistoIncapacitado() {
        return valorSinistoIncapacitado;
    }

    public void setValorSinistoIncapacitado(double valorSinistoIncapacitado) {
        this.valorSinistoIncapacitado = valorSinistoIncapacitado;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void toggleAtivo() {
        this.ativo = !this.ativo;
    }
    
    
}
