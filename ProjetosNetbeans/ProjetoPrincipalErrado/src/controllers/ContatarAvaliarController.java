/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTimePicker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import models.*;
import views.Alerta;

import java.awt.*;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author marti
 */
public class ContatarAvaliarController extends Controller {

    @FXML
    Label nSolicitacao;
    @FXML
    Label nomeCandidato;
    @FXML
    Label telefone;
    @FXML
    Label email;
    @FXML
    JFXTimePicker timePicker;
    @FXML
    DatePicker datePicker;

    private Candidato candidato;
    private SolicitacaoSeguro solicitacaoSeguro;


    public ContatarAvaliarController(SolicitacaoSeguro selectedItem) throws IOException {
        super("/views/ContatarAvaliar.fxml");
        solicitacaoSeguro = selectedItem;
        candidato = solicitacaoSeguro.getCandidato();

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        nSolicitacao.setText(solicitacaoSeguro.getNumeroSolicitacao() + "");
        nomeCandidato.setText(candidato.getNome());

        telefone.setText("" + candidato.getTelefone());
        email.setText(candidato.getEmail());
    }


    @FXML
    void acaoVoltar(ActionEvent event) throws IOException {
        super.setWindow(new ListaSolicitacoesAvaliarController());
    }

    @FXML
    void agendar(ActionEvent event) throws IOException {
        LocalDate v = datePicker.getValue();
        int dia, mes, ano, hora, minuto;
        try {
            dia = v.getDayOfMonth();
            mes = v.getMonthValue()-1;
            ano = v.getYear();
        } catch (Exception exception) {
            Alerta.mostrar("Erro ao agendar", "Coloque uma data valida", true);
            return;

        }
        LocalTime l = timePicker.getValue();
        try {
            hora = l.getHour();
            minuto = l.getMinute();
            System.out.println(minuto);

        } catch (Exception exception) {
            Alerta.mostrar("Erro ao agendar", "Coloque um horario valido", true);
            return;
        }
        solicitacaoSeguro.setDataVisitaCandidato(
                dia, mes, ano, hora, minuto
        );
        System.out.println("done-marcado");
        Alerta.mostrar("Concluído", "Consulta marcada com sucesso", true);
        acaoVoltar(event);
    }

    @FXML
    void mailto(ActionEvent event) {
        try {
            if (Desktop.isDesktopSupported()) {
                Desktop desktop = Desktop.getDesktop();
                if (desktop.isSupported(Desktop.Action.MAIL)) {
                    URI mailto = new URI(candidato.getEmail() + "subject=Marcar%20Horario%20da%20consulta%20da%20solicitacao%20do%20seguro%20de%20vida");
                    desktop.mail(mailto);
                }
            }
        } catch (URISyntaxException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    @FXML
    void verInfosCandidato(ActionEvent event) {
        Alerta.mostrar("Informações do Candidato: ", candidato.toString(), true);
    }
}
