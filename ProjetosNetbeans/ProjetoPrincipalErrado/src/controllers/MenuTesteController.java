/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 *  FMXL Controller Login
 * 
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class MenuTesteController extends Controller {

    public MenuTesteController() throws IOException {
        super("/views/MenuTeste.fxml");
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }
    
    @FXML
    private void solicitarSeguro(ActionEvent event) throws IOException {
        super.setWindow(new SolicitarSeguroController());
    }
    
    @FXML
    private void contratarSeguro(ActionEvent event) throws IOException{
        super.setWindow(new ContratarSeguroController());
    }
    @FXML
    void acaoVoltar(ActionEvent event) throws IOException {
        super.setWindow(new LoginController(true));
    }
}
