/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import exceptions.InvalidFieldException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import models.Contato;
import models.Parentesco;
import models.Sinistro;
import models.TextFieldFormatter;
import models.dao.ApoliceDao;
import views.Alerta;

/**
 * FXML Controller Relatar Morte
 *
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class RelatarMorteController extends Controller {

    @FXML
    private BorderPane jfxBorderPane_RelatarMorte;
    @FXML
    private JFXTextField jfxTxtField_NumeroApolice;
    @FXML
    private JFXTextField jfxTxtField_NomeCompletoSegurado;
    @FXML
    private JFXTextField jfxTxtField_CausaMorte;
    @FXML
    private JFXButton jfxBtn_AnexarObito;
    @FXML
    private Label label_NomeArquivo;
    @FXML
    private JFXTextField jfxTxtField_NomeCompletoContato;
    @FXML
    private JFXComboBox<String> jfxCombo_Parentesco;
    @FXML
    private JFXTextField jfxTxtField_Telefone;
    @FXML
    private JFXTextField jfxTxtField_Email;

    private File certidaoObito;

    private TextFieldFormatter mascaraNumero;
    private TextFieldFormatter mascaraTelefone;

    public RelatarMorteController() throws IOException {
        super("/views/RelatarMorte.fxml");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        for (Parentesco parentescoEnum : Parentesco.values()) {
            this.jfxCombo_Parentesco.getItems().add(parentescoEnum.getNome());
        }

        mascaraNumero = new TextFieldFormatter();
        mascaraNumero.setMask("##########");
        mascaraNumero.setCaracteresValidos("0123456789");
        mascaraNumero.setTf(jfxTxtField_NumeroApolice);

        mascaraTelefone = new TextFieldFormatter();
        mascaraTelefone.setMask("(##)#####-####");
        mascaraTelefone.setCaracteresValidos("0123456789");
        mascaraTelefone.setTf(jfxTxtField_Telefone);
    }

    @FXML
    private void acaoVoltar(ActionEvent event) throws IOException {
        System.out.println("Botão \"Voltar\" invocado.");
        super.setWindow(new LoginController());
    }

    @FXML
    private void acaoAnexar(ActionEvent event) {
        System.out.println("Botão \"Anexar óbito\" invocado.");
//        Alerta.mostrar("Esperando arquivo", new JFXSpinner());

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Selecione a certidão de óbito");
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("Arquivo PDF", "*.pdf")
        );

        File arquivoSelecionado = fileChooser.showOpenDialog(null);

        if (arquivoSelecionado != null) {
            Alerta.desaparecer();
            Alerta.mostrar("Sucesso!", "Arquivo importado com sucesso.", true);
            jfxBtn_AnexarObito.setStyle("-fx-background-color: #2f7929;");
            label_NomeArquivo.setText(arquivoSelecionado.getName());
            certidaoObito = arquivoSelecionado;
            return;
        }

        if (certidaoObito == null) {
            Alerta.desaparecer();
            Alerta.mostrar("Ops...", "Nenhum arquivo foi selecionado.", true);
        } else {
            Alerta.desaparecer();
            Alerta.mostrar("Ops...", "Nenhum arquivo foi selecionado será mantido o anterior.", true);
        }
    }

    @FXML
    private void acaoEnviar(ActionEvent event) {
        System.out.println("Botão \"Enviar\" invocado.");
        
        Contato contato = new Contato();
        Sinistro sinistro = new Sinistro();

        String validar = this.validarFormulario(sinistro, contato);
        if (validar != null) {
            Alerta.addBtn("OK", "red", "white", acao -> {});
            Alerta.mostrar("Verifique as informações", validar, true);
            return;
        }
        
//        ContatoDao contatoDao = new ContatoDao();
//        SinistroDao sinistroDao = new SinistroDao();
//        
//        try {
//            contatoDao.insert(contato);
//            contatoDao.getIdInserido();
//            
//            
//        } catch(IOException | IllegalStateException | SQLException e){
//            Alerta.mostrar("Erro inesperado", e.getMessage(), true);
//        }
                

//        for (Segurado segurado : segurados) {
//            if (segurado.getApoliceSeguro().getNumeroApolice() == numeroApolice) { // Apolice existe
//                if (segurado.getNome().equalsIgnoreCase(nomeCompletoSegurado)) { // Verifica se o nome informado confere com o que está no contrato
//                    if (segurado.getApoliceSeguro().isAtivo()) { // Verifica se a apolice está ativa
//                        Sinistro relatarMorte = new Sinistro(segurado.getApoliceSeguro(), causaMorte, certidaoObito, emailContato, parentesco, telefoneContato, emailContato);
//                        alerta.mostrar("Formulário enviado", "O formulário foi enviado com sucesso.");
//                        return;
//                    } else {
//                        alerta.mostrar("Apólice Inativa", "O número da apólice informada está inativa.");
//                        return;
//                    }
//                } else {
//                    alerta.mostrar("Informação inconsistente", "O nome informado não confere com o número da apólice.\nVerifique e tente novamente.");
//                    return;
//                }
//            } else {
//                alerta.mostrar("Apólice não encontrada", "O número da apólice informada não foi encontrada.\nVerifique e tente novamente.");
//            }
//        }
    }

    private String validarFormulario(Sinistro sinistro, Contato contato) {
        String mensagemAlerta = "";
        long numeroApolice = 0;
        
        List<String> errosSegurado = new ArrayList();
        List<String> errosContato = new ArrayList();

        // Verifica as informações do Segurado
        try {
            InvalidFieldException.validarNumero(this.jfxTxtField_NumeroApolice, "Número da apólice inválida.");
            numeroApolice = Long.parseLong(this.jfxTxtField_NumeroApolice.getText());
        } catch (InvalidFieldException e) {
            errosSegurado.add(e.getMessage());
        }

        try {
            InvalidFieldException.validarTexto(this.jfxTxtField_NomeCompletoSegurado, true, "Nome do segurado inválido.");
//            nomeCompletoSegurado = this.jfxTxtField_NomeCompletoSegurado.getText();
        } catch (InvalidFieldException e) {
            errosSegurado.add(e.getMessage());
        }
        
        try {
            InvalidFieldException.validarTexto(this.jfxTxtField_CausaMorte, false, "Informe a causa da morte.");
            sinistro.setCausaMorte(this.jfxTxtField_CausaMorte.getText());
        } catch (InvalidFieldException e) {
            errosSegurado.add(e.getMessage());
        }

        try {
            InvalidFieldException.validarArquivo(this.certidaoObito, "Insira uma Certidão de Óbito.");
            sinistro.setCertidaoObito(certidaoObito);
        } catch (InvalidFieldException e) {
            errosSegurado.add(e.getMessage());
        }

        // Verifica as informações do Contato
        try {
            InvalidFieldException.validarTexto(this.jfxTxtField_NomeCompletoContato, true, "Nome do contato inválido.");
            contato.setNome(this.jfxTxtField_NomeCompletoContato.getText());
        } catch (InvalidFieldException e) {
            errosContato.add(e.getMessage());
        }

        try {
            InvalidFieldException.validarComboBox(this.jfxCombo_Parentesco, "Selecione um parentesco.");
            contato.setParentesco(1);//this.jfxCombo_Parentesco.getValue()
        } catch (InvalidFieldException e) {
            errosContato.add(e.getMessage());
        }

        try {
            InvalidFieldException.validarTelefone(this.jfxTxtField_Telefone, "Informe um telefone válido.");
            contato.setTelefone(Long.parseLong(this.jfxTxtField_Telefone.getText().replaceAll("[^0-9]", "")));
        } catch (InvalidFieldException e) {
            errosContato.add(e.getMessage());
        }

        try {
            InvalidFieldException.validarEmail(jfxTxtField_Email, "Informe um email válido.");
            contato.setEmail(jfxTxtField_Email.getText());
        } catch (InvalidFieldException e) {
            errosContato.add(e.getMessage());
        }
        
        if (errosSegurado.size() > 0 || errosContato.size() > 0) {
            if (errosSegurado.size() > 0) {
                mensagemAlerta += "Informações do segurado\n";
                mensagemAlerta = errosSegurado.stream().map((erro) -> "   - " + erro + "\n").reduce(mensagemAlerta, String::concat);
                mensagemAlerta += "\n";
            }

            if (errosContato.size() > 0) {
                mensagemAlerta += "Informações do contato\n";
                mensagemAlerta = errosContato.stream().map((erro) -> "   - " + erro + "\n").reduce(mensagemAlerta, String::concat);
            }

            return mensagemAlerta;
        }
        
        ApoliceDao apolicedao = new ApoliceDao();
        try {
            mensagemAlerta = apolicedao.consultarPorNumero(numeroApolice);
        } catch(SQLException e){
            mensagemAlerta = e.getMessage();
        }
        
        
        //FAZER AQUI: CONSULTAR E VERIRICAR SE A APOLICE EXISTE, SE ESTA EM VIGÊNCIA, SE OS DADOS INFORMADOS BATEM.
        
        return mensagemAlerta;
    }

    @FXML
    private void mascaraNumero() {
        mascaraNumero.formatter();
    }

    @FXML
    private void mascaraTelefone() {
//        mascaraTelefone.formatter();
    }
}
