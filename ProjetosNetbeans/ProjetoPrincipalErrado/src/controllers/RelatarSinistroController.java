/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSpinner;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import views.Alerta;

/**
 * FXML Controller class
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class RelatarSinistroController extends Controller implements Initializable {
    
    @FXML private JFXTextField jfxTxtField_NumeroApolice;

    @FXML private JFXTextField jfxTxtField_NomeCompletoSegurado;

    @FXML private JFXTextArea jfxTextArea_descricao;
    
    @FXML private Label label_NomeArquivo;
    
    private File certidao;

    public RelatarSinistroController() throws IOException {
        super("/views/RelatarSinistro.fxml");
    }    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    
    @FXML
    private void acaoAnexar(ActionEvent event) {
        System.out.println("Botão \"Anexar certidão\" invocado.");
        //Alerta.mostrar("Esperando arquivo", new JFXSpinner());

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Selecione a certidão de óbito");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Arquivo PDF", "*.pdf")
        );

        File arquivoSelecionado = fileChooser.showOpenDialog(null);

        if (arquivoSelecionado != null) {
            Alerta.desaparecer();
            Alerta.mostrar("Sucesso!", "Arquivo importado com sucesso.", true);
            jfxTextArea_descricao.setStyle("-fx-background-color: #2f7929;");
            label_NomeArquivo.setText(arquivoSelecionado.getName());
            certidao = arquivoSelecionado;
            return;
        }
        
        if (certidao == null) {
            Alerta.desaparecer();
            Alerta.mostrar("Ops...", "Nenhum arquivo foi selecionado.", true);
        } else {
            Alerta.desaparecer();
            Alerta.mostrar("Ops...", "Nenhum arquivo foi selecionado será mantido o anterior.", true);
        }
    }
    
    @FXML
    void acaoEnviar(ActionEvent event) {
        System.out.println("Botão \"Enviar\" invocado.");
        
        if(certidao == null){
            Alerta.mostrar("Ops...", "Anexe uma certidão.", true);
            return;
        }
        
        if(jfxTextArea_descricao.getText().isEmpty()){
             Alerta.mostrar("Ops...", "Informe a descrição", true);
             return;
        }
        
        Alerta.mostrar("Sinistro enviado", "O sinistro foi enviado com sucesso.\nContactaremos em breve.", true);
    }
}
