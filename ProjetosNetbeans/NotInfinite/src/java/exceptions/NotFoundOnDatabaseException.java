package exceptions;

import java.sql.SQLException;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class NotFoundOnDatabaseException extends SQLException {

    public NotFoundOnDatabaseException(String message) {
        super(message);
    }
}
