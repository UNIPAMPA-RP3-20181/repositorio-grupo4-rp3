package models;

import java.util.Date;

public class Corretor extends Funcionario {
    public Corretor(String nome, String cpf, String email, String telefone, Date dataNascimento, char sexo, String cep, String cidade, String endereco, String bairro, Date dataContratacao, boolean ativo) {
        super(nome, cpf, email, telefone, dataNascimento, sexo, cep, cidade, endereco, bairro, dataContratacao, ativo);
    }
}
