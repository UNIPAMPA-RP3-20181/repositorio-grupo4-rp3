/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
abstract class DAO {
    
    protected Conexao conexao;

    public DAO() {
        this.conexao = Conexao.getInstance();
        this.conexao.init("localhost", 3306, "seguradora", "root", "caramboladev");
    }
}
