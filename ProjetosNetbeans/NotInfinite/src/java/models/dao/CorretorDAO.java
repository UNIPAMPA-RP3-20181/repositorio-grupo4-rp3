/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import models.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class CorretorDAO extends DAO implements ICorretorDAO {

    @Override
    public List<Corretor> getCorretores() throws SQLException {
        conexao.abrirConexao();
        List<Corretor> retorno = new ArrayList<>();
        PreparedStatement stmt = conexao.prepararDeclaracao("select pessoas.id, nome, cpf, email, telefone, "
                + "datanascimento, sexo, cep, endereco, cidade, bairro, datacontratacao, ativo "
                + "from funcionarios, pessoas, tipofuncionario, tipopessoa "
                + "where tipofuncionario.id=funcionarios.tipofuncionario "
                + "and pessoas.tipopessoa=`tipopessoa`.`id` "
                + "and tipopessoa.descricao=? "
                + "and tipofuncionario.descricao=? ");

        // ====== FAZ ISSO PELA PRÓPRIA CONSULTA ======
        // stmt.setString(1, "Funcionario");
        // stmt.setString(2, "Avaliador");
        
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            //id, nome, cpf, email, telefone, datanascimento, sexo, cep
            Corretor c = new Corretor(
                    rs.getString("nome"),
                    rs.getString("cpf"), //cpf
                    rs.getString("email"),
                    rs.getString("telefone"),
                    rs.getDate("datanascimento"), //nasc
                    rs.getString("sexo").toUpperCase().charAt(0), //sexo
                    rs.getString("cep"), //cep
                    rs.getString("cidade"),//cidadef
                    rs.getString("endereco"), //endereco
                    rs.getString("bairro"),//bairro
                    rs.getDate("datacontratacao"),
                    rs.getBoolean("ativo"));//bairro
            c.setId(rs.getInt("id"));

            retorno.add(c);
        }
        
        conexao.fecharConexao();
        return retorno;
    }

}
