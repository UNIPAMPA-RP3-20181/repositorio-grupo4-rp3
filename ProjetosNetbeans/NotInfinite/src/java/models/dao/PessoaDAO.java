/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import exceptions.NotFoundOnDatabaseException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import models.Pessoa;
import models.Session;
import models.Usuario;

/**
 *
 * @author martins
 */
public class PessoaDAO extends DAO implements IPessoaDAO {

    @Override
    public int adicionarPessoa(Pessoa pessoa) throws SQLException {
        conexao.abrirConexao();
        PreparedStatement stmt = null;

        String sql = "INSERT INTO pessoas (nome, cpf, email, telefone, datanascimento, sexo, cidade, endereco, bairro, cep, vivo, incapacitado) "
                + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";

        stmt = conexao.prepararDeclaracao(sql, Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1, pessoa.getNome());
        stmt.setString(2, pessoa.getCpf());
        stmt.setString(3, pessoa.getEmail());
        stmt.setString(4, pessoa.getTelefone());
        stmt.setDate(5, new Date(pessoa.getDataNascimento().getTime()));
        stmt.setString(6, String.valueOf(pessoa.getSexo()));
        stmt.setString(7, pessoa.getCidade());
        stmt.setString(8, pessoa.getEndereco());
        stmt.setString(9, pessoa.getBairro());
        stmt.setString(10, pessoa.getCep());
        stmt.setBoolean(11, true); 
        stmt.setBoolean(12, false);

        stmt.execute();

        ResultSet rs = stmt.getGeneratedKeys();

        int idInserido = 0;

        if (rs.next()) {
            idInserido = rs.getInt(1);
        }

        conexao.fecharConexao();
        
        adicionarVinculo(idInserido, 1);

        return idInserido;
    }

    @Override
    public Pessoa getPessoaPorId(int id) throws SQLException {
        System.out.println(id);
        conexao.abrirConexao();
        PreparedStatement stmt = conexao.prepararDeclaracao("select nome, cpf, email, telefone, "
                + "datanascimento, sexo, cep, cidade, endereco, bairro "
                + "from pessoas "
                + "where pessoas.id=? limit 1");
        stmt.setInt(1, id);

        ResultSet rs = stmt.executeQuery();
        if (!rs.first()) {
            throw new NotFoundOnDatabaseException("Pessoa não encontrada");
        }

        Pessoa p = new Pessoa(rs.getString("nome"),
                rs.getString("cpf"),
                rs.getString("email"),
                rs.getString("telefone"),
                rs.getDate("datanascimento"),
                rs.getString("sexo").charAt(0),
                rs.getString("cep"),
                rs.getString("cidade"),
                rs.getString("endereco"),
                rs.getString("bairro"));

        p.setId(id);

        PreparedStatement stmt2 = conexao.prepararDeclaracao("select descricao from tipopessoa, vinculo, pessoas "
                + "where pessoas.id=vinculo.idpessoa and tipopessoa.id=vinculo.idtipo and pessoas.id=?");

        stmt2.setInt(1, id);

        ResultSet rs2 = stmt2.executeQuery();
        while (rs2.next()) {
            p.addPrivilegio(rs2.getString("descricao"));
        }
        return p;
    }

    @Override
    public int updatePessoa(Pessoa pessoa) throws SQLException {
        conexao.abrirConexao();
        PreparedStatement stmt = null;
        pessoa.setId(Session.getInstance().getPessoa().getID());
        
        String sql = "UPDATE pessoas "
                   + "SET nome=?, telefone=?, email=?,cep=?, cidade=?, endereco=?, bairro=?"
                   + "WHERE pessoas.id=?";
        
        stmt = conexao.prepararDeclaracao(sql);
        stmt.setString(1, pessoa.getNome());
        stmt.setString(2, pessoa.getTelefone());
        stmt.setString(3, pessoa.getEmail());
        stmt.setString(4, pessoa.getCep());
        stmt.setString(5, pessoa.getCidade());
        stmt.setString(6, pessoa.getEndereco());
        stmt.setString(7, pessoa.getBairro());
        stmt.setInt(8, pessoa.getID());
        
        stmt.execute();
        conexao.fecharConexao();
        return pessoa.getID();
    }
    
    public void adicionarVinculo(int idpessoa, int idtipo) throws SQLException{
        conexao.abrirConexao();
        PreparedStatement stmt = null;

        String sql = "INSERT INTO vinculo (idpessoa, idtipo) "
                + "VALUES(?,?)";

        stmt = conexao.prepararDeclaracao(sql);
        stmt.setInt(1, idpessoa);
        stmt.setInt(2, idtipo);
        stmt.execute();

        conexao.fecharConexao();
    }

}
