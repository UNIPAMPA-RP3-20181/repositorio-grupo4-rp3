/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class Conexao {

    private static Conexao ME;
    private Connection conexao;

    private String caminho;
    private String usuario;
    private String senha;

    private Conexao() {
    }

    public static Conexao getInstance() {
        if (ME == null) {
            ME = new Conexao();
        }
        return ME;
    }

    public void init(String host, int porta, String banco, String usuario, String senha) {
        this.usuario = usuario;
        this.senha = senha;
        this.caminho = "jdbc:mysql://" + host + ':' + porta + '/' + banco;
    }

    public Connection abrirConexao() throws SQLException, IllegalStateException {
        if (caminho == null) {
            throw new IllegalStateException("Conexão com o banco não foi inicializada.");
        }

        return (conexao = DriverManager.getConnection(caminho, usuario, senha));
    }

    public void fecharConexao() throws SQLException {
        if (conexao != null) {
            conexao.close();
            conexao = null;
        }
    }

    public PreparedStatement prepararDeclaracao(String sql) throws SQLException {
        return conexao.prepareStatement(sql);
    }

    public PreparedStatement prepararDeclaracao(String sql, int statement) throws SQLException {
        return conexao.prepareStatement(sql, statement);
    }

    public Statement prepararDeclaracao() throws SQLException {
        return conexao.createStatement();
    }

}
