/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package models.dao;

import exceptions.NotFoundOnDatabaseException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import models.Pessoa;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class FuncionarioDAO extends DAO{

    public String getTipoFuncionario(Pessoa pessoa) throws SQLException {
        conexao.abrirConexao();
        
        int id = pessoa.getID();
        
        PreparedStatement stmt = conexao.prepararDeclaracao("select tipofuncionario.descricao as d "
                + "from tipofuncionario, funcionarios "
                + "where funcionarios.tipofuncionario=tipofuncionario.id "
                + "and funcionarios.idpessoa=? limit 1");
        
        stmt.setInt(1, id);
        System.out.println("id = "+ id);
        ResultSet rs = stmt.executeQuery();
        
        if(!rs.first())
            throw new NotFoundOnDatabaseException("Erro no DB, corrija");
        
        return rs.getString("d");
    }

}
