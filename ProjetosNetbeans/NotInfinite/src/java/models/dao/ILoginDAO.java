/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import exceptions.EmptyFieldException;
import exceptions.IncorrectPasswordException;
import exceptions.IncorrectUsernameException;
import exceptions.NullFieldException;
import java.sql.SQLException;
import models.Session;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
interface ILoginDAO {

    void autenticar(String user, String senha, Session session) throws SQLException, NullFieldException, EmptyFieldException, IncorrectUsernameException, IncorrectPasswordException;

}
