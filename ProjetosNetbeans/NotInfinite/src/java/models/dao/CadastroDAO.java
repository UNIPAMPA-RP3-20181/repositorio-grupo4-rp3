/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import exceptions.NotFoundOnDatabaseException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import models.Pessoa;
import models.Session;
import models.Usuario;

/**
 *
 * @author Michael Martins
 */
public class CadastroDAO extends DAO implements ICadastroDAO {

    @Override
    public void adicionarUsuario(Usuario usuario, Pessoa pessoa) throws SQLException {
        PessoaDAO pDAO = new PessoaDAO();
        int idPessoa = pDAO.adicionarPessoa(pessoa);
        
        conexao.abrirConexao();
        
        PreparedStatement stmt = null;

        // FOI ALTERADO
        stmt = conexao.prepararDeclaracao("INSERT INTO usuarios (idpessoa, username, password) VALUES(?,?,?)");
        
        stmt.setInt(1, idPessoa);
        stmt.setString(2, usuario.getUsuario());
        stmt.setString(3, usuario.getSenha());
        
        stmt.execute();
        
        conexao.fecharConexao();
    }

    @Override
    public void updateUsuario(Usuario usuario, Pessoa pessoa) throws SQLException {
        PessoaDAO pDAO = new PessoaDAO();
        int idPessoa = pDAO.updatePessoa(pessoa);
        
        conexao.abrirConexao();
        PreparedStatement stmt = null;

        // FOI ALTERADO
        stmt = conexao.prepararDeclaracao(
                "UPDATE usuarios "
              + "SET username=?, password=?"
              + "WHERE usuarios.idpessoa=?");
        
        stmt.setString(1, usuario.getUsuario());
        stmt.setString(2, usuario.getSenha());
        stmt.setInt(3, idPessoa);
        
        stmt.execute();
        pessoa.setId(idPessoa);
        conexao.fecharConexao();
        Session.getInstance().setPessoa(pessoa);
    }

    @Override
    public Usuario getUsuario(int idPessoa) throws SQLException {
        
        conexao.abrirConexao();

        String sql = "SELECT usuarios.idpessoa, usuarios.username, usuarios.password "
                   + "FROM usuarios, pessoas " 
                   + "WHERE pessoas.id = usuarios.idpessoa "
                        + "AND usuarios.idpessoa=?";
        
        PreparedStatement stmt = conexao.prepararDeclaracao(sql);
        
        stmt.setInt(1, idPessoa);
        
        stmt.execute();
        
        ResultSet rs = stmt.executeQuery();
        if (!rs.first()) {
            throw new NotFoundOnDatabaseException("Usuario não encontrado.");
        }
        
        Usuario user = new Usuario(
                rs.getString("username"), 
                Session.getInstance().getPessoa().getEmail(), 
                rs.getString("password"));
        
        conexao.fecharConexao();
        return user;
    }
    
}

