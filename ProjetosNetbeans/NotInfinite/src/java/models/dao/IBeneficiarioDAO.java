/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import java.sql.SQLException;
import java.util.List;
import models.Beneficiario;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
interface IBeneficiarioDAO {

    /**
     *
     * @param beneficiario Beneficiário a ser adicionado
     * @throws SQLException
     */
    void adicionarBeneficiario(Beneficiario beneficiario) throws SQLException;

    /**
     *
     * @param idPessoa ID do Candidato
     * @return Lista de beneficiários do candidato informado
     * @throws SQLException
     */
    List<Beneficiario> getBeneficiariosPorCandidato(Beneficiario beneficiario) throws SQLException;

}
