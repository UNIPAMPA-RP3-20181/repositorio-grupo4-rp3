/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import exceptions.NotFoundOnDatabaseException;
import models.Beneficiario;
import models.Candidato;

/**
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class CandidatoDAO extends DAO implements ICandidatoDAO {

    @Override
    public Candidato getCandidatoById(int id) throws SQLException {
        System.out.println("id candidato: " + id);
        conexao.abrirConexao();
        PreparedStatement stmt = conexao.prepararDeclaracao("select pessoas.id as id, nome, cpf, email, telefone, "
                + "datanascimento, sexo, cep, endereco, cidade, bairro "
                + "from pessoas, tipopessoa, vinculo where tipopessoa.id=vinculo.idtipo and vinculo.idpessoa=pessoas.id "
                + "and tipopessoa.descricao=? and pessoas.id=?");
        stmt.setString(1, "Candidato");
        stmt.setInt(2, id);
        ResultSet rs = stmt.executeQuery();

        if (!rs.first()) {
            throw new NotFoundOnDatabaseException("Candidato não encontrado");
        }

        Candidato c = new Candidato(
                rs.getString("nome"),
                rs.getString("cpf"), //cpf
                rs.getString("email"),
                rs.getString("telefone"),
                rs.getDate("datanascimento"), //nasc
                rs.getString("sexo").toUpperCase().charAt(0), //sexo
                rs.getString("cep"), //cep
                rs.getString("cidade"),//cidade
                rs.getString("endereco"), //endereco
                rs.getString("bairro"));//bairro
        c.setId(rs.getInt("id"));

        PreparedStatement stmtBen = conexao.prepararDeclaracao("select tipoparentesco.descricao as nomeparentesco, "
                + "pessoas.id, nome, cpf, email, telefone, datanascimento, sexo, cep, endereco, cidade, bairro "
                + "from beneficiarios, pessoas, tipoparentesco "
                + "where idbeneficiario=pessoas.id "
                + "and parentesco=tipoparentesco.id "
                + "and idcandidato=?");

        stmtBen.setInt(1, rs.getInt("id"));

        ResultSet rsBen = stmtBen.executeQuery();
        while (rsBen.next()) {
            Beneficiario b = new Beneficiario(
                    rsBen.getString("nomeparentesco"),
                    rsBen.getString("nome"),
                    rsBen.getString("cpf"), //cpf
                    rsBen.getString("email"),
                    rsBen.getString("telefone"),
                    rsBen.getDate("datanascimento"), //nasc
                    rsBen.getString("sexo").charAt(0), //sexo
                    rsBen.getString("cep"), //cep
                    rsBen.getString("cidade"),//cidade
                    rsBen.getString("endereco"), //endereco
                    rsBen.getString("bairro"));//bairro

            c.setId(rs.getInt("id"));
            c.addBeneficiario(b);
        }

        return c;
    }

    @Override
    public void atualizarCandidato(int idpessoa, int tipoPessoa) throws SQLException {
        conexao.abrirConexao();
        PreparedStatement stmt = null;

        String sql = "UPDATE vinculo SET idtipo = ? WHERE idpessoa = ?";

        stmt = conexao.prepararDeclaracao(sql);
        stmt.setInt(1, tipoPessoa);
        stmt.setInt(2, idpessoa);
        stmt.execute();
        
        conexao.fecharConexao();
    }

}