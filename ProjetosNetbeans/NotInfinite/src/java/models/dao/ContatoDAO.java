/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import models.Contato;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class ContatoDAO extends DAO implements IContatoDAO {

    @Override
    public void registrarContato(Contato contato) throws SQLException {
        conexao.abrirConexao();
        PreparedStatement stmt = null;

        String query = "INSERT INTO contatos (`nome`, `idtipoparentesco`, `email`, `telefone`) "
                + "VALUES (?, (SELECT tipoparentesco.id FROM tipoparentesco WHERE tipoparentesco.descricao = ?), ?, ?)--;";

        stmt = conexao.prepararDeclaracao(query, Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1, contato.getNome());
        stmt.setString(2, contato.getParentesco());
        stmt.setString(3, contato.getEmail());
        stmt.setString(4, contato.getTelefone());
        stmt.execute();

        ResultSet rs = stmt.getGeneratedKeys();

        if (rs.next()) {
            contato.setIdContato(rs.getInt(1));
        }

        conexao.fecharConexao();
    }

    public void destruir() {
        try {
            this.finalize();
            System.gc();
        } catch (Throwable ex) {
            Logger.getLogger(ContatoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
