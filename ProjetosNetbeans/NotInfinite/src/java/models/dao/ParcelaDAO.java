/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import models.ApoliceSeguro;
import models.Parcela;

/**
 *
 * @author Michael Martins <michaelmartins096@gmail.com>
 */
public class ParcelaDAO extends DAO implements IParcelaDAO {

    @Override
    public List<Parcela> getParcelas(ApoliceSeguro apoliceSeguro) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void registrarParcelas(ApoliceSeguro apoliceSeguro, int qntParcelas) throws SQLException {

        for (int i = 1; i <= qntParcelas; i++) {
            conexao.abrirConexao();

            PreparedStatement stmt = null;
            String sql
                    = "INSERT INTO parcelas (valor, datavencimento, datapgto, idapolice) "
                    + "VALUES ( "
                    + "    (SELECT apolices.premio / ? FROM apolices WHERE apolices.id = ?),  "
                    + "    (SELECT DATE_ADD(apolices.datacontratacao, INTERVAL 10+(31*?) DAY)  "
                    + "    	FROM apolices  "
                    + "    	WHERE apolices.id = ?),  "
                    + "    (SELECT DATE_ADD(apolices.datacontratacao, INTERVAL ? MONTH)  "
                    + "    	FROM apolices  "
                    + "    	WHERE apolices.id = ?), "
                    + "    ?);";

            stmt = conexao.prepararDeclaracao(sql);
            stmt.setInt(1, qntParcelas);
            stmt.setInt(2, apoliceSeguro.getId());
            stmt.setInt(3, i);
            stmt.setInt(4, apoliceSeguro.getId());
            stmt.setInt(5, i);
            stmt.setInt(6, apoliceSeguro.getId());
            stmt.setInt(7, apoliceSeguro.getId());

            stmt.execute();
            conexao.fecharConexao();
        }

    }

}
