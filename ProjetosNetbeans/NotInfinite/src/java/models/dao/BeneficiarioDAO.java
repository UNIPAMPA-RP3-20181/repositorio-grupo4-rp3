/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.Beneficiario;

/**
 *
 * @author sathelerdev
 */
public class BeneficiarioDAO extends DAO implements IBeneficiarioDAO {

    @Override
    public void adicionarBeneficiario(Beneficiario beneficiario) throws SQLException {
        conexao.abrirConexao();
        PreparedStatement stmt = conexao.prepararDeclaracao("INSERT INTO `segurodevida2`.`pessoas` (`nome`, `cpf`, `email`, `telefone`, `datanascimento`, `sexo`, `cep`, `cidade`, `endereco`, `bairro`, `tipopessoa`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");

//        stmt.setString(1, model1);
//        stmt.setString(2, "NOW()");
//        stmt.setString(3, "Causa da morte: " + sinistro.getCausaMorte());
        conexao.fecharConexao();
    }

    @Override
    public List<Beneficiario> getBeneficiariosPorCandidato(Beneficiario beneficiario) throws SQLException {
        conexao.abrirConexao();
        List<Beneficiario> retorno = new ArrayList<>();
        PreparedStatement stmt = conexao.prepararDeclaracao("SELECT tipoparentesco.descricao as parentesco, pessoas.id, pessoas.nome, pessoas.cpf, pessoas.email, pessoas.telefone, pessoas.datanascimento, pessoas.sexo, pessoas.cep, pessoas.cidade, pessoas.endereco, pessoas.bairro "
                + "FROM pessoas, beneficiarios, tipoparentesco "
                + "WHERE pessoas.id=? "
                + "AND beneficiarios.idcandidato = pessoas.id "
                + "AND beneficiarios.parentesco = tipoparentesco.id--;");

//        stmt.setInt(1, idPessoa);
        stmt.setInt(1, 1);

        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            Beneficiario b = new Beneficiario(
                    rs.getString("parentesco"),
                    rs.getString("nome"),
                    rs.getString("cpf"),
                    rs.getString("email"),
                    rs.getString("telefone"),
                    rs.getDate("datanascimento"),
                    rs.getString("sexo").charAt(0), //sexo
                    rs.getString("cep"),
                    rs.getString("cidade"),
                    rs.getString("endereco"),
                    rs.getString("bairro"));

            b.setId(rs.getInt("id"));

            retorno.add(b);
        }

        conexao.fecharConexao();
        return retorno;
    }
}
