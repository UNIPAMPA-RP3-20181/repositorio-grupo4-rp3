/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import java.sql.SQLException;
import models.CartaoCredito;

/**
 *
 * @author Michael Martins <michaelmartins096@gmail.com>
 */
public interface ICartaoCreditoDAO {
    int registrarCartaoCredito(int idBandeira, CartaoCredito cartaoCredito) throws SQLException;
    
    int getIdCartaoCredito(long numeroCartao) throws SQLException;
    
}
