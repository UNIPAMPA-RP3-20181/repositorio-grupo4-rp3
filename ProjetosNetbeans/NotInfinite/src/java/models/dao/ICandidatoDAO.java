/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import java.sql.SQLException;
import java.util.List;
import models.Candidato;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
interface ICandidatoDAO {

    public Candidato getCandidatoById(int id) throws SQLException;
    
    void atualizarCandidato(int idpessoa, int tipoPessoa) throws SQLException;
    
}
