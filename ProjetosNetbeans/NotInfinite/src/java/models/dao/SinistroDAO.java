/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import models.ApoliceSeguro;
import models.Contato;
import models.Sinistro;

/**
 *
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class SinistroDAO extends DAO implements ISinistroDAO {

    @Override
    public void registarSinistro(Sinistro sinistro) throws SQLException {
        ContatoDAO contatoDAO = new ContatoDAO();
        contatoDAO.registrarContato(sinistro.getContato());
        contatoDAO.destruir();

        conexao.abrirConexao();

        FileInputStream certidao = null;
        try {
            certidao = new FileInputStream(sinistro.getCertidao());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SinistroDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        PreparedStatement stmt = null;

        String query = "INSERT INTO sinistros (idapolice, idcontato, data, descricao, tiposinistro, certidao) "
                + "VALUES (?, ?, CURDATE(), ?, (SELECT tiposinistro.id FROM tiposinistro WHERE tiposinistro.descricao = ?), ?)--;";

        stmt = conexao.prepararDeclaracao(query);
        stmt.setLong(1, sinistro.getApolice().getId());
        stmt.setInt(2, sinistro.getContato().getIdContato());
        stmt.setString(3, sinistro.getCausaMorte());
        stmt.setString(4, sinistro.getTipoSinistro());
        stmt.setBlob(5, (InputStream) certidao, (int) sinistro.getCertidao().length());
        stmt.execute();
        conexao.fecharConexao();
    }

    @Override
    public ObservableList<Sinistro> buscarSinistros() throws SQLException {
        conexao.abrirConexao();
        ObservableList<Sinistro> retorno = FXCollections.observableArrayList();
        PreparedStatement stmt = conexao.prepararDeclaracao("SELECT sinistros.id, apolices.numeroapolice, pessoas.nome, apolices.datacontratacao, apolices.premio, apolices.valorsinistromorte, apolices.valorsinistroincapacitado, sinistros.descricao as causa, sinistros.certidao, sinistros.data, sinistros.tiposinistro as idtiposinistro, tiposinistro.descricao as tiposinistro, sinistros.autorizado, sinistros.pareceravaliador, sinistros.idavaliador, contatos.idcontato, contatos.nome as nomeContato, tipoparentesco.descricao as parentesco, contatos.email, contatos.telefone "
                + "FROM apolices, contatos, sinistros, tiposinistro, tipoparentesco, pessoas, usuarios "
                + "WHERE apolices.id = sinistros.idapolice "
                + "	AND sinistros.tiposinistro = tiposinistro.id "
                + "    AND contatos.idcontato = sinistros.idcontato "
                + "    AND tipoparentesco.id = contatos.idtipoparentesco "
                + "    AND pessoas.id = usuarios.idpessoa "
                + "    AND usuarios.idpessoa = apolices.idcandidato "
                + "ORDER BY sinistros.data ASC--");

        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            Sinistro sinistro = new Sinistro();
            sinistro.setIdSinistro(rs.getInt("id"));
            sinistro.setApolice(new ApoliceSeguro(rs.getLong("numeroapolice"), rs.getDate("datacontratacao"), rs.getDouble("premio"), rs.getDouble("valorsinistromorte"), rs.getDouble("valorsinistroincapacitado")));
            sinistro.getApolice().setNomeSegurado(rs.getString("nome"));
            sinistro.setCausaMorte(rs.getString("causa"));

            File certidao = null;
            try {
                certidao = File.createTempFile("certidao_", ".pdf");
                certidao.deleteOnExit();
            } catch (IOException ex) {
                Logger.getLogger(SinistroDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                FileOutputStream output = new FileOutputStream(certidao);
                System.out.println("Arquivo salvo em: " + certidao.getAbsolutePath());
                InputStream input = rs.getBinaryStream("certidao");
                byte[] buffer = new byte[1024];
                while (input.read(buffer) > 0) {
                    output.write(buffer);
                }

                output.close();
                input.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SinistroDAO.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(SinistroDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

            sinistro.setCertidao(certidao);
            sinistro.setData(rs.getDate("data"));
            switch (rs.getInt("idtiposinistro")) {
                case 1:
                    // MORTE
                    Contato contato = new Contato();
                    contato.setIdContato(rs.getInt("idcontato"));
                    contato.setNome(rs.getString("nomeContato"));
                    contato.setParentesco(rs.getString("parentesco"));
                    contato.setTelefone(rs.getString("telefone"));
                    contato.setEmail(rs.getString("email"));

                    sinistro.setContato(contato);
                default:
                // INVALIDEZ

            }

            if (rs.getInt("idavaliador") > 0) {
                PreparedStatement pstmt = conexao.prepararDeclaracao("SELECT pessoas.nome FROM pessoas, usuarios, funcionarios, sinistros WHERE pessoas.id = ? AND pessoas.id = usuarios.idpessoa AND usuarios.idpessoa = funcionarios.idpessoa AND funcionarios.idpessoa = sinistros.idavaliador--;");
                pstmt.setInt(1, rs.getInt("idavaliador"));
                ResultSet rsl = pstmt.executeQuery();
                sinistro.setIdAvaliador(rs.getInt("idavaliador"));
                if (rsl.next()) {
                    sinistro.setNomeAvaliador(rsl.getString("nome"));
                }
                sinistro.setParecerAvaliador(rs.getString("pareceravaliador"));
                sinistro.setAutorizado(rs.getBoolean("autorizado"));
            }

            sinistro.setTipoSinistro(rs.getString("tiposinistro"));
            retorno.add(sinistro);
        }

        conexao.fecharConexao();
        return retorno;
    }

    @Override
    public void avaliarSinistro(Sinistro sinistro, int avaliador, String parecer, boolean autorizado) throws SQLException {
        conexao.abrirConexao();

        PreparedStatement stmt = null;

        String query = "UPDATE sinistros SET "
                + "autorizado = ?, "
                + "pareceravaliador = ?, "
                + "idavaliador = ? "
                + "WHERE sinistros.id = ?--;";

        stmt = conexao.prepararDeclaracao(query);

        stmt.setBoolean(1, autorizado);
        stmt.setString(2, parecer);
        stmt.setInt(3, avaliador);
        stmt.setInt(4, sinistro.getIdSinistro());

        stmt.execute();

        conexao.fecharConexao();
    }
    
    public void destruir(){
        try {
            this.finalize();
            System.gc();
        } catch (Throwable ex) {
            Logger.getLogger(SinistroDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
