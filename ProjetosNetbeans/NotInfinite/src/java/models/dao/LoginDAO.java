package models.dao;

import exceptions.IncorrectUsernameException;
import exceptions.IncorrectPasswordException;
import exceptions.EmptyFieldException;
import exceptions.NotFoundOnDatabaseException;
import exceptions.NullFieldException;
import models.Session;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import models.Crypt;
import models.Pessoa;

public class LoginDAO extends DAO implements ILoginDAO {

    public void autenticar(String user, String senha, Session session) throws SQLException, NullFieldException, EmptyFieldException, IncorrectUsernameException, IncorrectPasswordException {

        if (senha == null || user == null) {
            throw new NullFieldException("Usuario ou senha nulos");
        }
        if (senha.isEmpty() || user.isEmpty()) {
            throw new EmptyFieldException("Usuário ou senha nulos");
        }

        conexao.abrirConexao();

        PreparedStatement stmt = conexao.prepararDeclaracao("select usuarios.idpessoa as id, pessoas.cpf as cpf "
                + "from usuarios, pessoas "
                + "where usuarios.idpessoa=pessoas.id "
                + "and usuarios.username=? limit 1");
        stmt.setString(1, user);

        System.out.println("RS1");

        ResultSet rs = stmt.executeQuery();


        if (!rs.first()) {
            throw new IncorrectUsernameException("Usuário Incorreto");
        }

        String cpf = rs.getString("cpf");
        System.out.println("cpf: "+cpf);
        String encryptedPass = Crypt.sha512(senha, cpf);

        System.out.println("encryptedPass: "+encryptedPass);
        stmt.close();
        rs.close();
        PreparedStatement stmt2 = conexao.prepararDeclaracao("select usuarios.idpessoa as id "
                + "from usuarios, pessoas "
                + "where usuarios.idpessoa=pessoas.id "
                + "and usuarios.username=? and BINARY usuarios.password=? limit 1");
        stmt2.setString(1, user);
        stmt2.setString(2, encryptedPass);
        
        
        System.out.println("RS2");
        ResultSet rs2 = stmt2.executeQuery();

        if (!rs2.first()) {
            throw new IncorrectPasswordException("Senha Incorreta");
        }
        

        int id = rs2.getInt("id");

        IPessoaDAO pd = new PessoaDAO();
        Pessoa p = pd.getPessoaPorId(id);

        session.setPessoa(p);

        conexao.fecharConexao();
    }
}
