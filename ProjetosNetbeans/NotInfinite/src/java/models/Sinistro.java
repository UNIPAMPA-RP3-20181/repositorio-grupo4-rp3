/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import exceptions.InvalidValueException;
import java.io.File;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class Sinistro {

    private int idSinistro;

    private ApoliceSeguro apolice;
    private String causaMorte;
    private File certidao;
    private Contato contato;
    private Date data;

    private String tipoSinistro;
    private int idAvaliador;
    private String nomeAvaliador;
    private String parecerAvaliador;
    private boolean autorizado;

    public ApoliceSeguro getApolice() {
        return apolice;
    }

    public void setApolice(ApoliceSeguro apolice) throws InvalidValueException {
        this.apolice = apolice;
    }

    public String getCausaMorte() {
        return causaMorte;
    }

    public void setCausaMorte(String causaMorte) throws InvalidValueException {
        InvalidValueException.validarTexto(causaMorte, false, "Informe a causa da morte.");
        this.causaMorte = causaMorte;
    }

    public File getCertidao() {
        return certidao;
    }

    public void setCertidao(File certidaoObito) throws InvalidValueException {
        InvalidValueException.validarArquivo(certidaoObito, "Insira uma certidão.");
        this.certidao = certidaoObito;
    }

    public Contato getContato() {
        return contato;
    }

    public void setContato(Contato contato) throws InvalidValueException {
        this.contato = contato;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Date getData() {
        return data;
    }

    public int getIdAvaliador() {
        return idAvaliador;
    }

    public void setIdAvaliador(int idAvaliador) {
        this.idAvaliador = idAvaliador;
    }

    public String getParecerAvaliador() {
        return parecerAvaliador;
    }

    public void setParecerAvaliador(String parecerAvaliador) {
        this.parecerAvaliador = parecerAvaliador;
    }

    public boolean isAutorizado() {
        return autorizado;
    }

    public void setAutorizado(boolean autorizado) {
        this.autorizado = autorizado;
    }

    public String getTipoSinistro() {
        return tipoSinistro;
    }

    public void setTipoSinistro(String tipoSinistro) {
        this.tipoSinistro = tipoSinistro;
    }

    public int getIdSinistro() {
        return idSinistro;
    }

    public void setIdSinistro(int idSinistro) {
        this.idSinistro = idSinistro;
    }

    public String getNomeAvaliador() {
        return nomeAvaliador;
    }

    public void setNomeAvaliador(String nomeAvaliador) {
        this.nomeAvaliador = nomeAvaliador;
    }

    public void destruir() {
        try {
            this.certidao.delete();
            this.finalize();
            System.gc();
        } catch (Throwable ex) {
            Logger.getLogger(Sinistro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void destruirSemApagar() {
        try {
            this.finalize();
            System.gc();
        } catch (Throwable ex) {
            Logger.getLogger(Sinistro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
