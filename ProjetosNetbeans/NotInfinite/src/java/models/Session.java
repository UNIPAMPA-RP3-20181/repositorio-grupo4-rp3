package models;

import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class Session {

    private static Session ME;
    private Pessoa pessoaLogada;

    public static Session getInstance() {
        if (ME == null) {
            ME = new Session();
        }
        return ME;
    }

    private Session() {
    }

    public void setPessoa(Pessoa p) {
        cleanSession();
        this.pessoaLogada = p;

    }

    public Pessoa getPessoa() {
        return pessoaLogada;
    }

    public String getTipoPessoa() {
        if (pessoaLogada instanceof Candidato) {
            return "candidato";
        }
        if (pessoaLogada instanceof Corretor) {
            return "corretor";
        }
        if (pessoaLogada instanceof Avaliador) {
            return "avaliador";
        } else {
            return "nenhum";
        }
    }

    public void cleanSession() {
        pessoaLogada = null;
    }

    public boolean makePessoaSession(int id, String tipopessoa) throws SQLException {
        
        return false;
    }

    
    
    /**
     * ESSA VERIFICAÇÃO PODE SER FEITA DIRETAMENTE NA HORA DE CRIAR A SESSÃO, AO
     * FAZER A CONSULTAR NO BANCO DE DADOS, EU ACHO
     */
    public void makeAvaliadorSession(int id) {
//        try {
//            this.setPessoa(AvaliadorFactory.getAvaliadorById(id));
//        } catch (NotFoundOnDBException e) {
//            e.printStackTrace();
//        }

    }

    public void makeCorretorSession(int id) {
//        try {
//            this.setPessoa(CorretorFactory.getCorretorById(id));
//        } catch (NotFoundOnDBException e) {
//            e.printStackTrace();
//        }

    }

    public void makeCandidatoSession(int id) {
//        try {
//            this.setPessoa(CandidatoFactory.getCandidatoById(id));
//        } catch (NotFoundOnDBException e) {
//            e.printStackTrace();
//        }
    }

}
