package viewmodel.panels;

import viewmodel.ViewModel;
import viewmodel.frames.FrameAvaliarCandidatoVM;
import viewmodel.frames.FrameAvaliarSolicitacaoVM;
import viewmodel.frames.FrameListarSinistrosVM;
import views.frames.FrameAvaliarCandidatoListaView;
import views.frames.FrameAvaliarSolicitacaoListaView;
import views.frames.FrameListarSinistrosView;
import views.panels.PainelView;

import java.io.IOException;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class PainelCorretorVM extends ViewModel {

    public PainelCorretorVM() {

    }

    public void showFrameAvaliarSolicitacao() throws IOException {
        ((PainelView) myView).setContentFrame(new FrameAvaliarSolicitacaoListaView(new FrameAvaliarSolicitacaoVM(), (PainelView) myView));
    }
}
