package viewmodel.panels;

import viewmodel.ViewModel;
import viewmodel.frames.FrameAvaliarCandidatoVM;
import views.frames.FrameAvaliarCandidatoListaView;
import views.panels.PainelView;

import java.io.IOException;
import viewmodel.frames.FrameListarSinistrosVM;
import views.frames.FrameListarSinistrosView;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class PainelAvaliadorVM extends ViewModel {

    public PainelAvaliadorVM() {

    }

    public void showFrameAvaliarCandidato() throws IOException {
        ((PainelView) myView).setContentFrame(new FrameAvaliarCandidatoListaView(new FrameAvaliarCandidatoVM(), (PainelView) myView));
    }

    public void showFrameListarSinistros() throws IOException {
        ((PainelView) myView).setContentFrame(new FrameListarSinistrosView(new FrameListarSinistrosVM(), (PainelView) myView));
    }
}
