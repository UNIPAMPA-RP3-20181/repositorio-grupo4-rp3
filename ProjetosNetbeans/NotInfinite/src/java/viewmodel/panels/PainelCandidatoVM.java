package viewmodel.panels;

import viewmodel.ViewModel;
import viewmodel.frames.FrameCandidatoListaVM;
import viewmodel.frames.FrameSolicitarSeguroVM;
import views.frames.FrameCandidatoListaSolicitacoesView;
import views.frames.FrameSolicitarSeguroView;
import views.panels.PainelView;
import java.io.IOException;
import viewmodel.frames.FrameContratarSeguroVM;
import viewmodel.frames.FrameContratoResumoVM;
import views.frames.FrameContratarSeguroView;
import views.frames.FrameContratoResumoView;

/**
 *
 * @author Michael Martins
 */
public class PainelCandidatoVM extends ViewModel {

    public PainelCandidatoVM() {

    }

    public void showFrameContratarSeguro() throws IOException {
        ((PainelView) myView).setContentFrame(new FrameContratoResumoView(new FrameContratoResumoVM(), (PainelView) myView));
    }

    public void showFrameSolicitarSeguro() throws IOException {
        ((PainelView) myView).setContentFrame(new FrameSolicitarSeguroView(new FrameSolicitarSeguroVM(), (PainelView) myView));

    }

    public void showFrameListaSolicitacoes() throws IOException {
        ((PainelView) myView).setContentFrame(new FrameCandidatoListaSolicitacoesView(new FrameCandidatoListaVM(), (PainelView) myView));
    }
}
