package viewmodel.panels;

import java.io.IOException;

import viewmodel.RelatarMorteVM;
import viewmodel.ViewModel;
import views.frames.FrameBeneficiariosView;
import views.panels.PainelView;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class PainelSeguradoVM extends ViewModel {

    public PainelSeguradoVM() {

    }

    public void frameBeneficiarios() throws IOException {
        ((PainelView) myView).setContentFrame(new FrameBeneficiariosView(new RelatarMorteVM(), (PainelView) myView));
    }
    
    
}
