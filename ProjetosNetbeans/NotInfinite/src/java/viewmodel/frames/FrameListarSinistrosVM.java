package viewmodel.frames;

import java.io.IOException;
import java.sql.SQLException;
import javafx.collections.ObservableList;
import models.Sinistro;
import models.dao.SinistroDAO;
import viewmodel.ViewModel;
import views.frames.Frame;
import views.frames.FrameAvaliarSinistroView;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class FrameListarSinistrosVM extends ViewModel {

    public ObservableList<Sinistro> recuperarSinistros() throws SQLException {
        SinistroDAO sinistros = new SinistroDAO();
        return sinistros.buscarSinistros();
    }

    public void avaliar(Sinistro sinistro) throws IllegalArgumentException, IOException {
        if (sinistro == null) {
            throw new IllegalArgumentException("Selecione um sinistro para ser avaliado.");
        }

        ((Frame) myView).getPainelView().setContentFrame(new FrameAvaliarSinistroView(new FrameAvaliarSinistroVM(), ((Frame) myView).getPainelView(), sinistro));
    }

}
