package viewmodel.frames;

import models.*;
import models.dao.ISolicitacaoSeguroDAO;
import models.dao.SolicitacaoSeguroDAO;
import viewmodel.ViewModel;
import views.frames.Frame;
import views.frames.FrameAvaliarCandidatoEditarView;
import views.frames.FrameAvaliarCandidatoVisualizarView;
import views.frames.FrameAvaliarCandidatoListaView;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

public class FrameAvaliarCandidatoVM extends ViewModel {

    private SolicitacaoSeguro solicitacaoSeguro;

    public void requestForSolicitacoes() throws SQLException {
        ISolicitacaoSeguroDAO ssd = new SolicitacaoSeguroDAO();
        ((FrameAvaliarCandidatoListaView) myView).setListaSolicitacoes(ssd.getSolicitacoesSeguroComConsultaMarcadaNaoAvaliada());
    }

    public void avaliar() throws IOException {
        SolicitacaoSeguro s = ((FrameAvaliarCandidatoListaView) myView).getSolicitacaoSelecionada();
        if (s != null) {
            solicitacaoSeguro = s;
            ((Frame) myView).getPainelView().setContentFrame(new FrameAvaliarCandidatoVisualizarView(this, ((Frame) myView).getPainelView()));
        }
    }

    //tela visualizar
    public void voltarParaLista() throws IOException {
        solicitacaoSeguro=null;
        ((Frame) myView).getPainelView().setContentFrame(new FrameAvaliarCandidatoListaView( this, ((Frame) myView).getPainelView()));
    }

    public void aprovarSolicitacao() throws SQLException, IOException {
        solicitacaoSeguro.aprovar();
        new SolicitacaoSeguroDAO().atualizarSolicitacao(solicitacaoSeguro);
        voltarParaLista();
    }

    public void reprovarSolicitacao(String motivo) throws SQLException, IOException {
        solicitacaoSeguro.reprovar(motivo);
        System.out.println(solicitacaoSeguro.getDataVisitaCandidato());
        new SolicitacaoSeguroDAO().atualizarSolicitacao(solicitacaoSeguro);
        voltarParaLista();
    }

    public void editarSolicitacao() throws IOException {
        ((Frame) myView).getPainelView().setContentFrame(
                new FrameAvaliarCandidatoEditarView(this, ((Frame) myView).getPainelView())
        );
    }

    public void requestFieldsValueSetOnViewScreen() {
        FrameAvaliarCandidatoVisualizarView view = (FrameAvaliarCandidatoVisualizarView) myView;

        view.setNomeCandidato(solicitacaoSeguro.getCandidato().getNome());
        view.setEnderecoCandidato(solicitacaoSeguro.getCandidato().getEndereco() + ", " + solicitacaoSeguro.getCandidato().getBairro() + ", " + solicitacaoSeguro.getCandidato().getCidade());
        view.setNumeroSolicitacao(solicitacaoSeguro.getNumeroSolicitacao() + "");
        view.setValorPremio(solicitacaoSeguro.getValorPremioString());
        view.setTelefoneCandidato(solicitacaoSeguro.getCandidato().getTelefone());
        view.setIsFumanteCandidato(solicitacaoSeguro.isFumante() ? "Sim" : "Não");
        view.setIsAlcoolistaCandidato(solicitacaoSeguro.isAlcoolista() ? "Sim" : "Não");
        view.setValorDesejado(solicitacaoSeguro.getValorSolicitacaoString());

        for (ProblemaSaude p : solicitacaoSeguro.getProblemasSaude()) {
            view.addProblema(p);
        }
        for (Predisposicoes p : solicitacaoSeguro.getPredisposicoes()) {
            view.addPredisposicao(p);
        }
        Pair<Boolean, String> p=solicitacaoSeguro.podeSerAprovada();
        if(!p.getFirst()){
            view.setAvaliarBtnDisabled(p.getSecond());
        }


        view.setDetalhesBtn("Detalhes do candidato:" ,solicitacaoSeguro.getCandidato().toString());

    }





    //tela editar
    public boolean salvarAlteracoes() throws SQLException, IOException {
        FrameAvaliarCandidatoEditarView view = (FrameAvaliarCandidatoEditarView) myView;


        HashMap<String, String> hm = view.getProblemas();
        solicitacaoSeguro.getProblemasSaude().clear();
        hm.forEach((s, s2) -> solicitacaoSeguro.adicionarProblema(new ProblemaSaude(s, s2)));

        solicitacaoSeguro.getPredisposicoes().clear();
        if (view.isSelectedPredCancer()) {
            solicitacaoSeguro.addPredisposicao(Predisposicoes.CANCER);
        }
        if (view.isSelectedPredCardio()) {
            solicitacaoSeguro.addPredisposicao(Predisposicoes.CORACAO);
        }
        if (view.isSelectedPredDegeneracao()) {
            solicitacaoSeguro.addPredisposicao(Predisposicoes.DEGENERACAO);
        }
        if (view.isSelectedPredDemencia()) {
            solicitacaoSeguro.addPredisposicao(Predisposicoes.DEMENCIA);
        }
        if (view.isSelectedPredDiabete()) {
            solicitacaoSeguro.addPredisposicao(Predisposicoes.DIABETE);
        }
        if (view.isSelectedPredHiper()) {
            solicitacaoSeguro.addPredisposicao(Predisposicoes.HIPERTENSAO);
        }
        if (view.isSelectedPredOsteo()) {
            solicitacaoSeguro.addPredisposicao(Predisposicoes.OSTEOPOROSE);
        }
        if (view.isSelectedPredPulmo()) {
            solicitacaoSeguro.addPredisposicao(Predisposicoes.PULMONAR);
        }

        solicitacaoSeguro.setFumante(view.isSelectedFumante());
        solicitacaoSeguro.setAlcoolista(view.isSelectedAlcoolista());

        solicitacaoSeguro.setValorSolicitacao(view.getValorDesejado());

        solicitacaoSeguro.calcularValorPremio(true);
        if(solicitacaoSeguro.getValorSolicitacao()<1){
            view.showAlert("ERRO EM CAMPO OBRIGATÓRIO", "Digite um valor maior que 0 no campo \"Valor desejado\"");
            return false;
        }
        solicitacaoSeguro.setAlterador(Session.getInstance().getPessoa());
        ((Frame) myView).getPainelView().setContentFrame(new FrameAvaliarCandidatoVisualizarView(this, ((Frame) myView).getPainelView()));
        new SolicitacaoSeguroDAO().salvarAlteracoesNaSolicitacao(solicitacaoSeguro);
        return true;
    }

    public void     requestFieldsValueSetOnEditScreen() {
        FrameAvaliarCandidatoEditarView view = (FrameAvaliarCandidatoEditarView) myView;

        view.setNomeCandidato(solicitacaoSeguro.getNomeCandidato());
        view.setNumeroSolicitacao(solicitacaoSeguro.getNumeroSolicitacao() + "");

        for (ProblemaSaude p : solicitacaoSeguro.getProblemasSaude()) {
            view.adicionarProblema(p.getNomeProblema(), p.getDescricaoProblema());
        }
        for (Predisposicoes p : solicitacaoSeguro.getPredisposicoes()) {
            switch (p) {
                case CANCER:
                    view.checkPredCancer(true);
                    break;
                case CORACAO:
                    view.checkPredCardio(true);
                    break;
                case DIABETE:
                    view.checkPredDiabete(true);
                    break;
                case DEMENCIA:
                    view.checkPredDemencia(true);
                    break;
                case PULMONAR:
                    view.checkPredPulmo(true);
                    break;
                case DEGENERACAO:
                    view.checkPredDegeneracao(true);
                    break;
                case HIPERTENSAO:
                    view.checkPredHiper(true);
                    break;
                case OSTEOPOROSE:
                    view.checkPredOsteo(true);
                    break;

            }
            view.checkAlcoolista(solicitacaoSeguro.isAlcoolista());
            view.checkFumante(solicitacaoSeguro.isFumante());

            System.out.println(solicitacaoSeguro.getValorSolicitacao());
            view.setValorDesejado(solicitacaoSeguro.getValorSolicitacao());
        }
    }

    public void pedirAlteracaoValorPremio() {
        FrameAvaliarCandidatoEditarView view = (FrameAvaliarCandidatoEditarView) myView;
        float r = solicitacaoSeguro.calcularValorPremio(view.getValorDesejado(), view.getTotalPredisposicoesSelecionadas(), view.isSelectedFumante(), view.isSelectedAlcoolista());
        view.setValorSimulado(r);
    }

    public void voltarParaInformacoes() throws IOException {
        ((Frame) myView).getPainelView().setContentFrame(new FrameAvaliarCandidatoVisualizarView(this, ((Frame) myView).getPainelView()));

    }
}
