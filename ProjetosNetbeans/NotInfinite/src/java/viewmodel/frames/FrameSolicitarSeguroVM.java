package viewmodel.frames;

import models.*;
import models.dao.CandidatoDAO;
import models.dao.ISolicitacaoSeguroDAO;
import models.dao.SolicitacaoSeguroDAO;
import viewmodel.ViewModel;
import views.View;
import views.frames.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;

public class FrameSolicitarSeguroVM extends ViewModel {

    private SolicitacaoSeguro solicitacaoSeguro;

    public FrameSolicitarSeguroVM() {

    }


    public void verificarPossibilidadeDeSolicitarSeguro() {
        try {
            Session.getInstance().setPessoa(new CandidatoDAO().getCandidatoById(Session.getInstance().getPessoa().getID()));
            solicitacaoSeguro = new SolicitacaoSeguro((Candidato) Session.getInstance().getPessoa());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Pair<Boolean, String> p = solicitacaoSeguro.podeSerAprovada();
        if (!p.getFirst()) {
            ((FrameSolicitarSeguroView) myView).exibirAlertaReprovacao(p.getSecond());
            ((FrameSolicitarSeguroView) myView).disablePage();
        }
    }

    public void enviarSolicitacao() throws SQLException {
        FrameSolicitarSeguroView view = (FrameSolicitarSeguroView) myView;

        HashMap<String, String> hm = view.getProblemas();

        if (solicitacaoSeguro.getProblemasSaude() != null) solicitacaoSeguro.getProblemasSaude().clear();

        hm.forEach((s, s2) -> solicitacaoSeguro.adicionarProblema(new ProblemaSaude(s, s2)));

        if (solicitacaoSeguro.getPredisposicoes() != null) solicitacaoSeguro.getPredisposicoes().clear();

        if (view.isSelectedPredCancer()) {
            solicitacaoSeguro.addPredisposicao(Predisposicoes.CANCER);
        }
        if (view.isSelectedPredCardio()) {
            solicitacaoSeguro.addPredisposicao(Predisposicoes.CORACAO);
        }
        if (view.isSelectedPredDegeneracao()) {
            solicitacaoSeguro.addPredisposicao(Predisposicoes.DEGENERACAO);
        }
        if (view.isSelectedPredDemencia()) {
            solicitacaoSeguro.addPredisposicao(Predisposicoes.DEMENCIA);
        }
        if (view.isSelectedPredDiabete()) {
            solicitacaoSeguro.addPredisposicao(Predisposicoes.DIABETE);
        }
        if (view.isSelectedPredHiper()) {
            solicitacaoSeguro.addPredisposicao(Predisposicoes.HIPERTENSAO);
        }
        if (view.isSelectedPredOsteo()) {
            solicitacaoSeguro.addPredisposicao(Predisposicoes.OSTEOPOROSE);
        }
        if (view.isSelectedPredPulmo()) {
            solicitacaoSeguro.addPredisposicao(Predisposicoes.PULMONAR);
        }

        solicitacaoSeguro.setDataSolicitacao(new Date());

        solicitacaoSeguro.setFumante(view.isSelectedFumante());
        solicitacaoSeguro.setAlcoolista(view.isSelectedAlcoolista());

        solicitacaoSeguro.setValorSolicitacao(view.getValorDesejado());

        solicitacaoSeguro.calcularValorPremio(true);
        Pair<Boolean, String> p = solicitacaoSeguro.podeSerAprovada();
        if (p.getFirst()) {
            if (solicitacaoSeguro.getValorSolicitacao() < 1)
                view.showAlert("ERRO EM CAMPO OBRIGATÓRIO", "Digite um valor maior que 0 no campo \"Valor desejado\"");
            view.disablePage();
            new SolicitacaoSeguroDAO().registrarSolicitacao(solicitacaoSeguro);
        } else {
            view.exibirAlertaReprovacao(p.getSecond());
        }
    }


    public void requestFieldsValueSetOnEditScreen() {
        FrameSolicitarSeguroView view = (FrameSolicitarSeguroView) myView;

        view.setNomeCandidato(solicitacaoSeguro.getNomeCandidato());
        view.setNumeroSolicitacao(solicitacaoSeguro.getNumeroSolicitacao() + "");

        for (ProblemaSaude p : solicitacaoSeguro.getProblemasSaude()) {
            view.adicionarProblema(p.getNomeProblema(), p.getDescricaoProblema());
        }
        for (Predisposicoes p : solicitacaoSeguro.getPredisposicoes()) {
            switch (p) {
                case CANCER:
                    view.checkPredCancer(true);
                    break;
                case CORACAO:
                    view.checkPredCardio(true);
                    break;
                case DIABETE:
                    view.checkPredDiabete(true);
                    break;
                case DEMENCIA:
                    view.checkPredDemencia(true);
                    break;
                case PULMONAR:
                    view.checkPredPulmo(true);
                    break;
                case DEGENERACAO:
                    view.checkPredDegeneracao(true);
                    break;
                case HIPERTENSAO:
                    view.checkPredHiper(true);
                    break;
                case OSTEOPOROSE:
                    view.checkPredOsteo(true);
                    break;

            }
            view.checkAlcoolista(solicitacaoSeguro.isAlcoolista());
            view.checkFumante(solicitacaoSeguro.isFumante());

            System.out.println(solicitacaoSeguro.getValorSolicitacao());
            view.setValorDesejado(solicitacaoSeguro.getValorSolicitacao());
        }
    }

    public void pedirAlteracaoValorPremio() {
        FrameSolicitarSeguroView view = (FrameSolicitarSeguroView) myView;
        float r = solicitacaoSeguro.calcularValorPremio(view.getValorDesejado(), view.getTotalPredisposicoesSelecionadas(), view.isSelectedFumante(), view.isSelectedAlcoolista());
        view.setValorSimulado(r);
    }

    public void voltarParaInicio() throws IOException {
        ((Frame) myView).getPainelView().setContentFrame(new EmptyFrame(((Frame) myView).getPainelView()));

    }
}
