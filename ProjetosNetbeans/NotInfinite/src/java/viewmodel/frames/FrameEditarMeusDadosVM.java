package viewmodel.frames;

import java.sql.SQLException;
import models.Pessoa;
import models.Session;
import models.Usuario;
import models.dao.CadastroDAO;
import viewmodel.ViewModel;

/**
 *
 * @author Michael Martins
 */
public class FrameEditarMeusDadosVM extends ViewModel {

    public void atualizarUser(Usuario user, Pessoa pessoa) throws SQLException {
        CadastroDAO cDAO = new CadastroDAO();
        cDAO.updateUsuario(user, pessoa);
    }

    public Usuario recuperarUsuario(int idPessoa) throws SQLException {
        CadastroDAO cDAO = new CadastroDAO();
        Usuario user = cDAO.getUsuario(idPessoa);
        return user;
    }
}
