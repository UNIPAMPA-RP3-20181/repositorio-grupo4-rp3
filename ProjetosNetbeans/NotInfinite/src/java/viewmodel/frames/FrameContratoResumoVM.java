package viewmodel.frames;

import java.io.IOException;
import java.sql.SQLException;
import models.Session;
import models.SolicitacaoSeguro;
import models.dao.SolicitacaoSeguroDAO;
import viewmodel.ViewModel;
import viewmodel.panels.PainelCandidatoVM;
import views.frames.Frame;
import views.frames.FrameContratarSeguroView;
import views.panels.PainelCandidatoView;

/**
 *
 * @author Michael Martins
 */
public class FrameContratoResumoVM extends ViewModel {

    public FrameContratoResumoVM() {

    }

    public SolicitacaoSeguro recuperarSolicitacao(int idPessoa) throws SQLException {
        SolicitacaoSeguroDAO sDAO = new SolicitacaoSeguroDAO();
        SolicitacaoSeguro ss = sDAO.getSolicitacaoPorCandidato(idPessoa);
        return ss;
    }

    public void showFrameContratarSeguro(SolicitacaoSeguro seguro) throws IOException {
        ((Frame) myView).getPainelView().setContentFrame(new FrameContratarSeguroView(new FrameContratarSeguroVM(), ((Frame) myView).getPainelView()));
    }

    public void cancelar() throws IOException {
        new PainelCandidatoView(new PainelCandidatoVM()).show();
    }
    
    public boolean temSolicitacaoAprovada() throws SQLException{
        SolicitacaoSeguroDAO sdao = new SolicitacaoSeguroDAO();
        return sdao.temSolicitacaoAprovada(Session.getInstance().getPessoa().getID());
    }

}
