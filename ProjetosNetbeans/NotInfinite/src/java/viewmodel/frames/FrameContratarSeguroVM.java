package viewmodel.frames;

import java.io.IOException;
import java.sql.SQLException;
import models.ApoliceSeguro;
import models.CartaoCredito;
import models.Session;
import models.SolicitacaoSeguro;
import models.dao.ApoliceDAO;
import models.dao.CandidatoDAO;
import models.dao.CartaoCreditoDAO;
import models.dao.ParcelaDAO;
import models.dao.SolicitacaoSeguroDAO;
import viewmodel.ViewModel;
import views.frames.Frame;
import views.frames.FrameContratoResumoView;

/**
 *
 * @author Michael Martins
 */
public class FrameContratarSeguroVM extends ViewModel {

    private SolicitacaoSeguro ss;

    public double recuperarPremio() throws SQLException {
        return ss.getValorPremio();
    }

    public void voltar() throws IOException {
        ((Frame) myView).getPainelView().setContentFrame(new FrameContratoResumoView(new FrameContratoResumoVM(), ((Frame) myView).getPainelView()));
    }

    public void finalizar(int idBandeira, int quantidadeParcelas, CartaoCredito cc) throws SQLException, IOException {
        
        
        //Registro do Cartão de Crédito
        CartaoCreditoDAO ccDAO = new CartaoCreditoDAO();
        int idCartao = ccDAO.registrarCartaoCredito(idBandeira, cc);
        SolicitacaoSeguro seguro = ss;

        //Registro da Apólice
        ApoliceDAO aDAO = new ApoliceDAO();
        ApoliceSeguro as = aDAO.registrarApolice(seguro, idCartao);
        
        //Registro da(s) Parcela(s)
        ParcelaDAO pDAO = new ParcelaDAO();
        pDAO.registrarParcelas(as, quantidadeParcelas); 
        
        //Alteração do Vínculo do Candidato para Segurado
        CandidatoDAO cDAO = new CandidatoDAO();
        cDAO.atualizarCandidato(ss.getCandidato().getID(), 2);
        
    }

    public void setSolicitacaoAtual() throws SQLException {
        SolicitacaoSeguroDAO sDAO = new SolicitacaoSeguroDAO();
        ss = sDAO.getSolicitacaoPorCandidato(Session.getInstance().getPessoa().getID());
    }
    
    public SolicitacaoSeguro getSolicitacaotAtual() throws SQLException{
        return ss;
    }
    
}
