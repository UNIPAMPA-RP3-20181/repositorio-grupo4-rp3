/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewmodel;

import exceptions.InvalidValueException;
import exceptions.NotFoundOnDatabaseException;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javafx.stage.FileChooser;
import models.ApoliceSeguro;
import models.Contato;
import models.Sinistro;
import models.dao.ApoliceDAO;
import models.dao.ParentescoDAO;
import models.dao.SinistroDAO;
import views.LoginView;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class RelatarMorteVM extends ViewModel {

    private File certidao;

    public RelatarMorteVM() {
    }

    public void telaLogin() throws IOException {
        new LoginView(new LoginVM()).show();
    }

    public List<String> recuperarParentescos() throws SQLException {
        ParentescoDAO parentescos = new ParentescoDAO();
        return parentescos.buscarParentescos();
    }

    public void registrarMorte(String numeroApoliceText, String nomeCompletoSeguradoText, String causaMorte, String nomeCompletoContato, String parentesco, String telefone, String email) throws InvalidValueException, SQLException, NotFoundOnDatabaseException {
        Sinistro sinistro = new Sinistro();
        Contato contato = new Contato();

        List<String> errosContato = this.validarContato(contato, nomeCompletoContato, parentesco, telefone, email);
        List<String> errosSegurado = this.validarSinistro(sinistro, causaMorte);

        String nomeCompletoSegurado = null;
        try {
            InvalidValueException.validarNomeCompleto(nomeCompletoSeguradoText, "Nome do segurado inválido.");
            nomeCompletoSegurado = nomeCompletoSeguradoText;
        } catch (InvalidValueException e) {
            errosSegurado.add(0, e.getMessage());
        }

        long numeroApolice = 0;
        try {
            InvalidValueException.validarNumero(numeroApoliceText.trim(), "Número da apólice inválida.");
            numeroApolice = Long.parseLong(numeroApoliceText.trim());
            InvalidValueException.validarQuantidadeDigitos(numeroApolice, 10, "Número da apólice inválida.");
        } catch (InvalidValueException e) {
            errosSegurado.add(0, e.getMessage());
        }

        if (errosSegurado.size() > 0 || errosContato.size() > 0) {
            sinistro.destruirSemApagar();
            contato.destruir();
            this.gerarMensagemAlerta(errosSegurado, errosContato);
        }

        ApoliceDAO apoliceDAO = new ApoliceDAO();
        ApoliceSeguro apolice = apoliceDAO.recuperarApolice(numeroApolice, nomeCompletoSegurado);
        apoliceDAO.destruir();
        
        sinistro.setContato(contato);
        sinistro.setApolice(apolice);

        SinistroDAO sinistoDAO = new SinistroDAO();
        sinistoDAO.registarSinistro(sinistro);
        sinistoDAO.destruir();
        
        contato.destruir();
        sinistro.destruirSemApagar();
    }

    public String anexarCertidao() throws IllegalArgumentException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Selecione a certidão de óbito");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Arquivo PDF", "*.pdf")
        );

        File arquivoSelecionado = fileChooser.showOpenDialog(null);

        if (arquivoSelecionado != null) {
            this.certidao = arquivoSelecionado;
            return this.certidao.getName();
        }

        if (this.certidao == null) {
            throw new IllegalArgumentException("Nenhum arquivo foi selecionado.");
        } else {
            throw new IllegalArgumentException("Nenhum arquivo foi selecionado será mantido o anterior.");
        }
    }

    private List<String> validarContato(Contato contato, String nomeCompletoContato, String parentesco, String telefone, String email) {
        List<String> errosContato = new ArrayList();

        // Verifica as informações do Contato
        try {
            contato.setNome(nomeCompletoContato);
        } catch (InvalidValueException e) {
            errosContato.add(e.getMessage());
        }

        try {
            contato.setParentesco(parentesco);
        } catch (InvalidValueException e) {
            errosContato.add(e.getMessage());
        }

        try {
            contato.setTelefone(telefone.replaceAll("[^0-9]", ""));
        } catch (InvalidValueException e) {
            errosContato.add(e.getMessage());
        }

        try {
            contato.setEmail(email);
        } catch (InvalidValueException e) {
            errosContato.add(e.getMessage());
        }

        return errosContato;
    }

    private List<String> validarSinistro(Sinistro sinistro, String causaMorte) throws SQLException, NotFoundOnDatabaseException {
        List<String> errosSegurado = new ArrayList();

        try {
            sinistro.setCausaMorte(causaMorte);
        } catch (InvalidValueException e) {
            errosSegurado.add(e.getMessage());
        }

        try {
            sinistro.setCertidao(this.certidao);
        } catch (InvalidValueException e) {
            errosSegurado.add(e.getMessage());
        }
        
        sinistro.setTipoSinistro("Morte");

        return errosSegurado;
    }

    private void gerarMensagemAlerta(List<String> errosSegurado, List<String> errosContato) throws InvalidValueException {
        String mensagemAlerta = "";
        if (errosSegurado.size() > 0) {
            mensagemAlerta += "Informações do segurado\n";
            mensagemAlerta = errosSegurado.stream().map((erro) -> "\t- " + erro + "\n").reduce(mensagemAlerta, String::concat);
            mensagemAlerta += "\n";
        }

        if (errosContato.size() > 0) {
            mensagemAlerta += "Informações do contato\n";
            mensagemAlerta = errosContato.stream().map((erro) -> "\t- " + erro + "\n").reduce(mensagemAlerta, String::concat);
        }

        throw new InvalidValueException(mensagemAlerta);
    }
}
