package views.frames;

import com.jfoenix.controls.JFXButton;
import java.io.IOException;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import models.Crypt;
import models.Pessoa;
import models.Session;
import models.TextFieldFormatter;
import models.Usuario;
import viewmodel.ViewModel;
import viewmodel.frames.FrameEditarMeusDadosVM;
import viewmodel.panels.PainelCandidatoVM;
import views.Alerta;
import views.panels.PainelCandidatoView;
import views.panels.PainelView;

/**
 *
 * @author Michael Martins
 */
public class FrameEditarMeusDadosView extends Frame {

    @FXML
    private TextField nomeCompleto, nroTelefone, CEP, endereco, cidade;

    @FXML
    private TextField bairro;

    @FXML
    private TextField usuario, email;

    @FXML
    private PasswordField senhaAntiga, senhaNova, confSenha;

    @FXML
    private JFXButton btnCancelar, btnAtualizar;

    private String crypt;

    public FrameEditarMeusDadosView(ViewModel viewModel, PainelView painelView) {
        super(viewModel, painelView);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        carregarDados();
    }

    @FXML
    void atualizar(ActionEvent event) throws SQLException, IOException {
        if (validarCampos()) {
            Pessoa pessoaOLD = Session.getInstance().getPessoa();

            Pessoa p = new Pessoa(
                    nomeCompleto.getText(),
                    pessoaOLD.getCpf(),
                    email.getText(),
                    nroTelefone.getText(),
                    pessoaOLD.getDataNascimento(),
                    pessoaOLD.getSexo(),
                    CEP.getText(),
                    cidade.getText(),
                    endereco.getText(),
                    bairro.getText());

            Usuario user = new Usuario(
                    usuario.getText(),
                    email.getText(),
                    this.crypt);

            new FrameEditarMeusDadosVM().atualizarUser(user, p);

            Alerta alerta = new Alerta();
            alerta.addBtn("OK", acao -> {
                try {
                    new PainelCandidatoView(new PainelCandidatoVM()).show();
                } catch (IOException ex) {
                    Logger.getLogger(FrameContratoResumoView.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            alerta.exibir("ALTERAÇÃO", "Dados atualizados!");
            
        }
    }

    @FXML
    void cancelar(ActionEvent event) throws IOException {
        new PainelCandidatoView(new PainelCandidatoVM()).show();
    }

    @FXML
    void maskCEP(KeyEvent event) {
        TextFieldFormatter mask = new TextFieldFormatter();
        mask.setMask("#####-###");
        mask.setCaracteresValidos("0123456789");
        mask.setTf(CEP);
        mask.formatter();
    }

    @FXML
    void maskNroTelefone(KeyEvent event) {
        TextFieldFormatter mask = new TextFieldFormatter();
        mask.setMask("(##)#####-####");
        mask.setCaracteresValidos("0123456789");
        mask.setTf(nroTelefone);
        mask.formatter();
    }

    private boolean validarCampos() throws SQLException {
        if (nomeCompleto.getText().trim().equals("")) {
            alert("Atenção", "Informe seu nome completo.");
            return false;
        }

        if (TemDigitosExcessivos(nomeCompleto.getText())) {
            alert("Atenção", "Nome completo não pode conter mais de 80 caracteres.");
            return false;
        }

        if (nroTelefone.getText().trim().equals("")) {
            alert("Atenção", "Informe um número de telefone válido.");
            return false;
        }

        if (CEP.getText().trim().equals("")) {
            alert("Atenção", "Informe um CEP válido.");
            return false;
        }

        if (endereco.getText().trim().equals("")) {
            alert("Atenção", "Informe seu endereço.");
            return false;
        }

        if (TemDigitosExcessivos(endereco.getText())) {
            alert("Atenção", "Endereço não pode conter mais de 80 caracteres.");
            return false;
        }

        if (usuario.getText().trim().equals("")) {
            alert("Atenção", "Informe um nome de usuário.");
            return false;
        }

        if (TemDigitosExcessivos(usuario.getText())) {
            alert("Atenção", "Usuario não pode conter mais de 80 caracteres.");
            return false;
        }

        if (usuario.getText().contains(" ")) {
            alert("Atenção", "Nome de Usuario não pode conter espaços.");
            return false;
        }
        if (email.getText().trim().equals("")) {
            alert("Atenção", "Informe um e-mail de usuário.");
            return false;
        }

        if (!email.getText().trim().contains("@")) {
            alert("Atenção", "Informe um e-mail válido.");
            return false;
        }

        if (TemDigitosExcessivos(email.getText())) {
            alert("Atenção", "E-mail não pode conter mais de 80 caracteres.");
            return false;
        }

        return naoAlterouSenha();
    }

    private void alert(String titulo, String msg) {
        if (this.getRoot() != null) {
            new Alerta().exibir(titulo, msg);
        } else {
            System.out.println("é nulo, meu caro!");
        }
    }

    private boolean TemDigitosExcessivos(String txt) {
        return txt.length() > 80;
    }

    private void carregarDados() {
        try {
            Pessoa p = Session.getInstance().getPessoa();
            nomeCompleto.setText(p.getNome());
            nroTelefone.setText(p.getTelefone());
            CEP.setText(p.getCep());
            endereco.setText(p.getEndereco());
            bairro.setText(p.getBairro());
            cidade.setText(p.getCidade());
            email.setText(p.getEmail());

            Usuario user = ((FrameEditarMeusDadosVM) myViewModel).recuperarUsuario(Session.getInstance().getPessoa().getID());
            usuario.setText(user.getUsuario());
        } catch (Exception e) {
            alert("Opa!", "Ocorreu um erro enquanto carregava seus dados. Tente novamente mais tarde.");
        }
    }

    private boolean naoAlterouSenha() throws SQLException {
        Usuario userOLD = (new FrameEditarMeusDadosVM()).recuperarUsuario(Session.getInstance().getPessoa().getID());

        if (!senhaAntiga.getText().equals("")) {
            if (Crypt.sha512(senhaAntiga.getText(), Session.getInstance().getPessoa().getCpf()).equals(userOLD.getSenha())) {
                this.crypt = userOLD.getSenha();
            } else {
                alert("Atenção", "Esta não é sua senha antiga.");
                return false;
            }

            if ((!senhaNova.getText().equals("")) && (!confSenha.getText().equals(""))) {
                if (senhaNova.getText().equals(confSenha.getText())) {
                    this.crypt = Crypt.sha512(senhaNova.getText(), Session.getInstance().getPessoa().getCpf());
                } else {
                    alert("Atenção", "A senha nova e de confimação não são iguais.");
                    return false;
                }
            } else {
                alert("Atenção", "Informe sua senha nova/confirmação de senha.");
                return false;
            }
        } else {
            this.crypt = userOLD.getSenha();

        }

        return true;
    }
}
