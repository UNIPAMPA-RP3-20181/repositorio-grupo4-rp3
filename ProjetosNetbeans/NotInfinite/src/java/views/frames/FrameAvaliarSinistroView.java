/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.frames;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXToggleButton;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import models.Session;
import models.Sinistro;
import viewmodel.ViewModel;
import viewmodel.frames.FrameAvaliarSinistroVM;
import views.Alerta;
import views.panels.PainelView;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class FrameAvaliarSinistroView extends Frame {

    @FXML
    private Label sinistroLbl;

    @FXML
    private TextField numeroApoliceIpt;

    @FXML
    private TextField nomeSeguradoIpt;

    @FXML
    private TextField descricaoIpt;

    @FXML
    private VBox boxContato;

    @FXML
    private TextField nomeContatoIpt;

    @FXML
    private TextField parentescoIpt;

    @FXML
    private TextField telefoneIpt;

    @FXML
    private TextField emailIpt;

    @FXML
    private TextField nomeAvaliadorIpt;

    @FXML
    private JFXTextArea parecerTxt;

    @FXML
    private JFXToggleButton autorizadoTgg;
    @FXML
    private JFXButton cancelarBtn;

    @FXML
    private JFXButton confirmarBtn;

    private final Sinistro sinistro;

    public FrameAvaliarSinistroView(ViewModel viewModel, PainelView painelView, Sinistro sinistro) {
        super(viewModel, painelView);
        this.sinistro = sinistro;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.sinistroLbl.setText("Sinistro - " + this.sinistro.getTipoSinistro());
        this.numeroApoliceIpt.setText(String.valueOf(this.sinistro.getApolice().getNumeroApolice()));
        this.nomeSeguradoIpt.setText(this.sinistro.getApolice().getNomeSegurado());
        this.descricaoIpt.setText(this.sinistro.getCausaMorte());

        if (this.sinistro.getContato() != null) {
            this.boxContato.setVisible(true);
            this.nomeContatoIpt.setText(this.sinistro.getContato().getNome());
            this.parentescoIpt.setText(this.sinistro.getContato().getParentesco());
            String telefone = this.sinistro.getContato().getTelefone();
            this.telefoneIpt.setText(String.format("(%s) %s-%s", telefone.substring(0, 2), telefone.substring(2, 7), telefone.substring(7, 11)));
            this.emailIpt.setText(this.sinistro.getContato().getEmail());
        }

        if (this.sinistro.getIdAvaliador() > 0) {
            this.nomeAvaliadorIpt.setText(this.sinistro.getNomeAvaliador());
            this.parecerTxt.setText(this.sinistro.getParecerAvaliador());
            this.autorizadoTgg.setSelected(this.sinistro.isAutorizado());

            this.confirmarBtn.setVisible(false);
            this.cancelarBtn.setText("Voltar");
            this.parecerTxt.setEditable(false);
            this.autorizadoTgg.setDisable(true);
        } else {
            this.nomeAvaliadorIpt.setText(Session.getInstance().getPessoa().getNome().toUpperCase());
        }

    }

    @FXML
    private void confirmarBtnClick() {
        Alerta alerta = new Alerta();
        alerta.addBtn("Não", "#bf3d3d", "white", acao -> {

        });
        alerta.addBtn("Sim", "#3d7f22", "white", acao -> {
            alerta.fechar();
            try {
                ((FrameAvaliarSinistroVM) myViewModel).avaliarSinistro(this.sinistro, this.parecerTxt.getText(), this.autorizadoTgg.isSelected());
                new Alerta().exibir("Sucesso!", "O sinistro foi avaliado com sucesso.");
                ((FrameAvaliarSinistroVM) myViewModel).cancelar(this.sinistro);
            } catch (IllegalArgumentException ex) {
                new Alerta().exibir("Atenção!", ex.getMessage());
            } catch (IOException | SQLException ex) {
                new Alerta().exibir("Ops...", ex.getMessage());
            }
        });

        alerta.exibir("Atenção!", "Este sinistro foi definido como " + (this.autorizadoTgg.isSelected() ? "autorizado." : "não autorizado.") + "\n\nApós a confirmação, esta ação não poderá ser desfeita. Deseja continuar?");
    }

    @FXML
    private void cancelarBtnClick() {
        try {
            ((FrameAvaliarSinistroVM) myViewModel).cancelar(this.sinistro);
        } catch (IOException ex) {
            Logger.getLogger(FrameAvaliarSinistroView.class.getName()).log(Level.SEVERE, null, ex);
            new Alerta().exibir("Ops...", ex.getMessage());
        }
    }

    @FXML
    private void visualizarCertidaoBtnClick() {
        try {
            ((FrameAvaliarSinistroVM) myViewModel).visualizarCertidao(sinistro.getCertidao());
        } catch (IOException ex) {
            new Alerta().exibir("Ops...", ex.getMessage());
        }
    }
}
