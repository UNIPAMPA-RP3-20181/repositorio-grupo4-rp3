/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.frames;

import com.jfoenix.controls.JFXCheckBox;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import models.SolicitacaoSeguro;
import viewmodel.frames.FrameCandidatoListaVM;
import views.panels.PainelView;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * FXML Controller class
 *
 * @author marti
 */
public class FrameCandidatoListaHistoricoView extends Frame {

    //////////////////
    @FXML
    VBox problemsContainer;
    @FXML
    TableView<SolicitacaoSeguro> tv;

    private Set<JFXCheckBox> cbSet = new HashSet<JFXCheckBox>();

    public FrameCandidatoListaHistoricoView(FrameCandidatoListaVM viewModel, PainelView painelView) {
        super(viewModel, painelView);
    }


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        (tv.getColumns().get(0)).setCellValueFactory(new PropertyValueFactory<>("numeroSolicitacao"));

        (tv.getColumns().get(1)).setCellValueFactory(new PropertyValueFactory<>("dataSolicitacao"));
        (tv.getColumns().get(2)).setCellValueFactory(new PropertyValueFactory<>("nomeAlterador"));

        (tv.getColumns().get(3)).setCellValueFactory(new PropertyValueFactory<>("valorPremioString"));
        (tv.getColumns().get(4)).setCellValueFactory(new PropertyValueFactory<>("valorSolicitacaoString"));
        (tv.getColumns().get(5)).setCellValueFactory(new PropertyValueFactory<>("isIsvalida"));

        try {
            ((FrameCandidatoListaVM) myViewModel).requestForHistoricorSolicitacoes();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void setListaSolicitacoes(List<SolicitacaoSeguro> ls){
        tv.getItems().addAll(ls);
    }
    @FXML
    void cancelarBtnClick(ActionEvent event) throws IOException {
        ((FrameCandidatoListaVM) myViewModel).voltarParaLista();
    }

    @FXML
    void visualizar(ActionEvent event) throws IOException {
        ((FrameCandidatoListaVM) myViewModel).exibirInformacoesSolicitacaoSelecionada();

    }

    public SolicitacaoSeguro getSolicitacaoSelecionada() {
        return tv.getSelectionModel().getSelectedItem();
    }
}
