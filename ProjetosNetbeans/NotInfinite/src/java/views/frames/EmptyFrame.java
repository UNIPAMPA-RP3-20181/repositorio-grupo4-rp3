package views.frames;

import viewmodel.ViewModel;
import views.View;
import views.frames.Frame;
import views.panels.PainelView;

import java.net.URL;
import java.util.ResourceBundle;

public class EmptyFrame extends Frame {

    public EmptyFrame(PainelView painelView){
        super(new ViewModel() {
            @Override
            public void setView(View view) {
                super.setView(view);
            }
        }, painelView);

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //do nothing, just a blank page
    }
}
