package views.frames;

import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import models.Pessoa;
import models.Session;
import models.SolicitacaoSeguro;
import viewmodel.frames.FrameContratoResumoVM;
import views.Alerta;
import views.panels.PainelView;

/**
 *
 * @author Michael Martins
 */
public class FrameContratoResumoView extends Frame {

    @FXML
    private TextField nsolicitacao, datasolicitacao, valordasolicitacao, valordopremio, nomecandidato, fumante, alcoolista;

    @FXML
    private TextField nascimento, CPF, telefone, CEP, endereco, bairro, cidade;

    @FXML
    private JFXButton cancelar, contratarseguro;

    private SolicitacaoSeguro seguro;

    public FrameContratoResumoView(FrameContratoResumoVM viewModel, PainelView painelView) {
        super(viewModel, painelView);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {

            boolean temSolicitacoesAprovadas = ((FrameContratoResumoVM) myViewModel).temSolicitacaoAprovada();

            if (temSolicitacoesAprovadas) {
                //recuperando pessoa
                Pessoa c = Session.getInstance().getPessoa();

                SolicitacaoSeguro ss = null;

                //Recuperando solicitação
                ss = ((FrameContratoResumoVM) myViewModel).recuperarSolicitacao(c.getID());

                nsolicitacao.setText("" + ss.getNumeroSolicitacao());
                datasolicitacao.setText(ss.getDataSolicitacaoString());
                valordasolicitacao.setText(ss.getValorSolicitacaoString());
                valordopremio.setText(ss.getValorPremioString());
                nomecandidato.setText(Session.getInstance().getPessoa().getNome());
                if (ss.isFumante()) {
                    fumante.setText("SIM");
                } else {
                    fumante.setText("NÃO");
                }
                if (ss.isAlcoolista()) {
                    alcoolista.setText("SIM");
                } else {
                    alcoolista.setText("NÃO");
                }
                nascimento.setText(c.getDataNascimento().toString());
                telefone.setText(c.getTelefone());
                CPF.setText(c.getCpf());
                CEP.setText(c.getCep());
                endereco.setText(c.getEndereco());
                bairro.setText(c.getBairro());
                cidade.setText(c.getCidade());
            } else {
                Alerta alerta = new Alerta();
                alerta.addBtn("OK", acao -> {
                    try {
                        ((FrameContratoResumoVM) myViewModel).cancelar();
                    } catch (IOException ex) {
                        Logger.getLogger(FrameContratoResumoView.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                alerta.exibir("ATENÇÃO", "Você não possui solicitações aprovadas no momento. Tente novamente mais tarde.");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FrameContratoResumoView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void cancelarBtnClick(ActionEvent event) throws IOException {
        ((FrameContratoResumoVM) myViewModel).cancelar();
    }

    @FXML
    void contratarSeguroBtnClick(ActionEvent event) {
        try {
            ((FrameContratoResumoVM) myViewModel).showFrameContratarSeguro(seguro);

        } catch (IOException ex) {
            Logger.getLogger(FrameContratoResumoView.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

}
