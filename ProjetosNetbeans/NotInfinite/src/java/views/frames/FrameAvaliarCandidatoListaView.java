/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.frames;

import com.jfoenix.controls.JFXCheckBox;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import models.*;
import viewmodel.ViewModel;
import viewmodel.frames.FrameAvaliarCandidatoVM;
import views.panels.PainelView;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;

/**
 * FXML Controller class
 *
 * @author marti
 */
public class FrameAvaliarCandidatoListaView extends Frame {

    //////////////////
    @FXML
    VBox problemsContainer;
    @FXML
    TableView<SolicitacaoSeguro> tv;

    private Set<JFXCheckBox> cbSet = new HashSet<JFXCheckBox>();

    public FrameAvaliarCandidatoListaView(FrameAvaliarCandidatoVM viewModel, PainelView painelView) {
        super(viewModel, painelView);
    }


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        (tv.getColumns().get(0)).setCellValueFactory(new PropertyValueFactory<>("numeroSolicitacao"));

        (tv.getColumns().get(1)).setCellValueFactory(new PropertyValueFactory<>("nomeCandidato"));

        (tv.getColumns().get(2)).setCellValueFactory(new PropertyValueFactory<>("dataVisitaCandidato"));

        try {
            ((FrameAvaliarCandidatoVM) myViewModel).requestForSolicitacoes();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void setListaSolicitacoes(List<SolicitacaoSeguro> ls){
        tv.getItems().addAll(ls);


    }
    @FXML
    void acaoVoltar(ActionEvent event) throws IOException {
    }

    @FXML
    void avaliar(ActionEvent event) throws IOException {
        ((FrameAvaliarCandidatoVM) myViewModel).avaliar();

    }

    public SolicitacaoSeguro getSolicitacaoSelecionada() {
        return tv.getSelectionModel().getSelectedItem();
    }
}
