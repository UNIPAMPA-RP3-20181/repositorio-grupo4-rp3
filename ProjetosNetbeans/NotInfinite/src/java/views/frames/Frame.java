package views.frames;

import viewmodel.ViewModel;
import views.panels.PainelView;
import views.View;

import java.io.IOException;

public abstract class Frame extends View {
    private PainelView painelView;

    public Frame(ViewModel viewModel, PainelView painelView) {
        super(viewModel);
        this.painelView=painelView;

    }

    protected void changeFrame(Frame frame) throws IOException {
        System.gc();
        painelView.setContentFrame(frame);
    }
    public PainelView getPainelView() {
        return painelView;


    }
}
