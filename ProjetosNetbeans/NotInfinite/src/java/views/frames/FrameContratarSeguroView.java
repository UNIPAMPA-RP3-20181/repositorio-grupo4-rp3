package views.frames;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import models.CartaoCredito;
import models.TextFieldFormatter;
import viewmodel.ViewModel;
import viewmodel.frames.FrameContratarSeguroVM;
import viewmodel.panels.PainelSeguradoVM;
import views.Alerta;
import views.panels.PainelSeguradoView;
import views.panels.PainelView;

/**
 *
 * @author Michael Martins
 */
public class FrameContratarSeguroView extends Frame {

    @FXML
    private JFXTextField textFieldNCartao, textFieldNTitular, textFieldMes, textFieldAno, textFieldCVV;
    @FXML
    private JFXComboBox<String> comboBoxParcelas, comboBoxBandeira;
    @FXML
    private Label premio;
    @FXML
    private JFXButton cancelarBtnClick, finalizarBtnClick;

    private double valor = 0;

    ObservableList<String> observableListParcelas;

    ObservableList<String> observableListBandeira;

    public FrameContratarSeguroView(ViewModel viewModel, PainelView painelView) {
        super(viewModel, painelView);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {

            ((FrameContratarSeguroVM) myViewModel).setSolicitacaoAtual();

            NumberFormat formatarValor = new DecimalFormat("#0.00");
            double premioRecuperado = ((FrameContratarSeguroVM) myViewModel).recuperarPremio();
            premio.setText("" + formatarValor.format(premioRecuperado));
            valor = premioRecuperado;

            observableListParcelas = FXCollections.observableArrayList(
                    "1x " + formatarValor.format(valor), "2x " + formatarValor.format(valor / 2), "3x " + formatarValor.format(valor / 3),
                    "4x " + formatarValor.format(valor / 4), "5x " + formatarValor.format(valor / 5), "6x " + formatarValor.format(valor / 6),
                    "7x " + formatarValor.format(valor / 7), "8x " + formatarValor.format(valor / 8), "9x " + formatarValor.format(valor / 9),
                    "10x " + formatarValor.format(valor / 10), "11x " + formatarValor.format(valor / 11), "12x " + formatarValor.format(valor / 12));
            comboBoxParcelas.setItems(observableListParcelas);

            observableListBandeira = FXCollections.observableArrayList(
                    "American Express", "Diners Club", "Visa", "MasterCard", "Elo", "Discover");
            comboBoxBandeira.setItems(observableListBandeira);

        } catch (SQLException ex) {
            Logger.getLogger(FrameContratarSeguroView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void maskAno(KeyEvent event) {
        TextFieldFormatter mask = new TextFieldFormatter();
        mask.setMask("##");
        mask.setCaracteresValidos("0123456789");
        mask.setTf(textFieldAno);
        mask.formatter();
    }

    @FXML
    void maskMes(KeyEvent event) {
        TextFieldFormatter mask = new TextFieldFormatter();
        mask.setMask("##");
        mask.setCaracteresValidos("0123456789");
        mask.setTf(textFieldMes);
        mask.formatter();
    }

    @FXML
    void maskNCartao(KeyEvent event) {
        TextFieldFormatter mask = new TextFieldFormatter();
        mask.setMask("####-####-####-####");
        mask.setCaracteresValidos("0123456789");
        mask.setTf(textFieldNCartao);
        mask.formatter();
    }

    @FXML
    void maskCVV(KeyEvent event) {
        TextFieldFormatter mask = new TextFieldFormatter();
        mask.setMask("###");
        mask.setCaracteresValidos("0123456789");
        mask.setTf(textFieldCVV);
        mask.formatter();
    }

    @FXML
    void cancelarContrato(ActionEvent event) {
        try {
            ((FrameContratarSeguroVM) myViewModel).voltar();
        } catch (IOException ex) {
            Logger.getLogger(FrameContratarSeguroView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void finalizar(ActionEvent event) throws SQLException, IOException {
        if (validarCampos()) {
            int idBandeira = comboBoxBandeira.getSelectionModel().getSelectedIndex() + 1;
            int quantidadeParcelas = comboBoxParcelas.getSelectionModel().getSelectedIndex() + 1;

            CartaoCredito cc = cartaoValido();

            ((FrameContratarSeguroVM) myViewModel).finalizar(idBandeira, quantidadeParcelas, cc);
            Alerta alerta = new Alerta();
            alerta.addBtn("OK", acao -> {
                try {
                    new PainelSeguradoView(new PainelSeguradoVM()).show();
                } catch (IOException ex) {
                    Logger.getLogger(FrameContratarSeguroView.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            alerta.exibir("Olá Segurado!", "Seu contrato foi realizado com sucesso.");

        }
    }

    private boolean validarCampos() {
        if (textFieldNCartao.getText().trim().equals("")) {
            Alert("Informe seu número do cartão.");
            return false;
        }

        if (textFieldNTitular.getText().trim().equals("")) {
            Alert("Informe o nome do titular do cartão.");
            return false;
        }

        if (textFieldMes.getText().trim().equals("")) {
            Alert("Informe o mês do cartão.");
            return false;
        }

        if (textFieldAno.getText().trim().equals("")) {
            Alert("Informe o ano do cartão.");
            return false;
        }

        if (textFieldCVV.getText().trim().equals("")) {
            Alert("Informe o CVV do cartão.");
            return false;
        }

        if (textFieldCVV.getText().trim().length() != 3) {
            Alert("O Código de Verificação do Cartão (CVV) deve conter 3 dígitos.");
            return false;
        }

        if (comboBoxParcelas.getValue() == null) {
            Alert("Selecione a quantidade de parcelas desejada.");
            return false;
        }

        if (comboBoxBandeira.getValue() == null) {
            Alert("Selecione a bandeira do cartão.");
            return false;
        }
        return verificaVencimento();
    }

    private void Alert(String msg) {
        new Alerta().exibir("Atenção", msg);
    }

    private CartaoCredito cartaoValido() {
        long numero = Long.parseLong(textFieldNCartao.getText().trim().replace("-", ""));
        String nome = textFieldNTitular.getText();
        int ano = Integer.parseInt(textFieldAno.getText().trim());
        int mes = Integer.parseInt(textFieldMes.getText().trim());
        Date vencimento = dateFormat(ano, mes);
        int cvv = Integer.parseInt(textFieldCVV.getText());

        CartaoCredito cc = new CartaoCredito(numero, nome, vencimento, cvv);
        return cc;
    }

    private Date dateFormat(int ano, int mes) {
        int tempAno = 2000 + ano;
        Date nDate = new Date(ano + 100, mes - 1, 1);
        return nDate;
    }

    private boolean verificaVencimento() {
        Date dataAtual = new Date();

        int mes = Integer.parseInt(textFieldMes.getText().trim());
        int ano = Integer.parseInt(textFieldAno.getText().trim());

        Date dateVencimento = dateFormat(ano, mes);

        if (mes > 12) {
            Alert("Informe um Mês válido");
            return false;
        }

        if (dateVencimento.before(dataAtual)) {
            Alert("Cartão Vencido! Por favor informe um válido.");
            return false;
        }

        return true;
    }

}
