package views.frames;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import models.Beneficiario;
import models.dao.BeneficiarioDAO;
import viewmodel.ViewModel;
import views.panels.PainelView;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class FrameBeneficiariosView extends Frame {

    @FXML
    private TableView<Beneficiario> tabelaBeneficiarios;

    public FrameBeneficiariosView(ViewModel viewModel, PainelView painelView) {
        super(viewModel, painelView);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tabelaBeneficiarios.setEditable(false);
        (tabelaBeneficiarios.getColumns().get(0)).setCellValueFactory(new PropertyValueFactory("nome"));
        
        // ERRADISSIMO!!!!!!
        List<Beneficiario> beneficiarios = new ArrayList();
        BeneficiarioDAO beneficiariosDAO = new BeneficiarioDAO();

//        try {
//            beneficiarios = beneficiariosDAO.getBeneficiariosPorCandidato();
//        } catch (SQLException ex) {
//            Logger.getLogger(ListarBeneficiariosController.class.getName()).log(Level.SEVERE, null, ex);
//        }
        final ObservableList<Beneficiario> data = FXCollections.observableArrayList(beneficiarios);

///        tabelaBeneficiarios.setItems(data);
    }

    @FXML
    void acaoAdicionar(ActionEvent event) {

    }

    @FXML
    void acaoEditar(ActionEvent event) {

    }

    @FXML
    void acaoRemover(ActionEvent event) {

    }

}
