/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.frames;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableRow;
import javafx.scene.control.cell.PropertyValueFactory;
import models.Predisposicoes;
import models.ProblemaSaude;
import viewmodel.frames.FrameCandidatoListaVM;
import views.Alerta;
import views.panels.PainelView;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

//import models.mocks.MockCandidato;
//import models.mocks.MockSolicitacaoSeguro1;
//import models.mocks.MockSolicitacaoSeguro2;

/**
 * FXML Controller class
 *
 * @author marti
 */
public class FrameCandidatoVisualizarSolicitacaoView extends Frame {

    @FXML
    private javafx.scene.control.TableView<ProblemaSaude> problemaSaudeTableView;
    @FXML
    private javafx.scene.control.TableView<Predisposicoes> predisposicoesTableView;
    @FXML
    private Label  idSolicitacaoLbl, valorPremioLbl,
            isFumanteLbl, isAlcoolistaLbl, valorDesejadoLbl;
    @FXML
    private JFXButton editarBtn;



    private final ObservableList<ProblemaSaude> prob = FXCollections.observableArrayList();
    private final ObservableList<Predisposicoes> pred = FXCollections.observableArrayList();

    public FrameCandidatoVisualizarSolicitacaoView(FrameCandidatoListaVM viewModel, PainelView painelView) {
        super(viewModel, painelView);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {


        problemaSaudeTableView.getColumns().clear();
        problemaSaudeTableView.setEditable(true);
        javafx.scene.control.TableColumn nome = new javafx.scene.control.TableColumn("Nome");
        nome.setCellValueFactory(new PropertyValueFactory<ProblemaSaude, String>("nomeProblema"));
        javafx.scene.control.TableColumn descricao = new javafx.scene.control.TableColumn("Descricao");
        descricao.setCellValueFactory(new PropertyValueFactory<ProblemaSaude, String>("descricaoProblema"));
        problemaSaudeTableView.setItems(prob);
        problemaSaudeTableView.getColumns().addAll(nome, descricao);


        problemaSaudeTableView.setRowFactory(tv -> {
            TableRow<ProblemaSaude> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    new Alerta().exibir("Infromações do problema", row.getItem().toString());
                }
            });
            return row;
        });


        predisposicoesTableView.getColumns().clear();
        predisposicoesTableView.setEditable(true);
        javafx.scene.control.TableColumn nomep = new javafx.scene.control.TableColumn("Nome");
        nomep.setCellValueFactory(new PropertyValueFactory<Predisposicoes, String>("nome"));
        predisposicoesTableView.setItems(pred);
        predisposicoesTableView.getColumns().addAll(nomep);


        ((FrameCandidatoListaVM) myViewModel).requestFieldsValueSet();
    }


    public void addProblema(ProblemaSaude p) {
        prob.add(p);
    }

    public void addPredisposicao(Predisposicoes p) {
        pred.add(p);
    }


    public void setEditarBtnDisabled() {
        editarBtn.setDisable(true);
    }

    @FXML
    void cancelarBtnClick(ActionEvent event) throws IOException {
        ((FrameCandidatoListaVM) myViewModel).voltarParaHisorico();
    }

    @FXML
    void editarBtnClick(ActionEvent event) throws IOException {
        ((FrameCandidatoListaVM) myViewModel).editarSolicitacao();
    }







    public void setNumeroSolicitacao(String s) {
        idSolicitacaoLbl.setText(s);
    }

    public void setValorPremio(String s) {
        valorPremioLbl.setText(s);
    }

    public void setIsFumanteCandidato(String s) {
        isFumanteLbl.setText(s);
    }

    public void setIsAlcoolistaCandidato(String s) {
        isAlcoolistaLbl.setText(s);
    }

    public void setValorDesejado(String s) {
        valorDesejadoLbl.setText(s);
    }

}
