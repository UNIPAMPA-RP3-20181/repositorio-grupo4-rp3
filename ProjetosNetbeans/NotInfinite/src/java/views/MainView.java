/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.StackPane;
import viewmodel.ViewModel;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
class MainView extends View {

    @FXML
    private StackPane rootView;
    private Node initialNode;

    public MainView(ViewModel viewModel, Node initialNode) {
        super(viewModel);
        this.initialNode = initialNode;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        View.setRootView(rootView);
        rootView.getChildren().add(initialNode);
    }
}
