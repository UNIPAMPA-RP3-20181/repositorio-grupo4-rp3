/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.panels;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXPopup;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import models.Session;
import viewmodel.LoginVM;
import viewmodel.frames.FrameEditarMeusDadosVM;
import viewmodel.ViewModel;
import views.LoginView;
import views.View;
import views.frames.Frame;
import views.frames.FrameEditarMeusDadosView;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public abstract class PainelView extends View {

    @FXML
    protected JFXButton menuUsuarioBtn;

    @FXML
    protected Label tituloFrameLbl;

    @FXML
    protected FontAwesomeIconView iconeTituloFrame;

    @FXML
    protected VBox frame;

    @FXML
    protected VBox menuLateral;

    public PainelView(ViewModel viewModel) {
        super(viewModel);
        setFXML(getClass().getSuperclass().getName().split("\\.")[getClass().getName().split("\\.").length - 1].replaceAll("View", ""));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        JFXListView<Label> list = new JFXListView<>();
        JFXPopup popup = new JFXPopup(list);
        Label meusDadosUsuario = new Label("Meus dados", GlyphsDude.createIcon(FontAwesomeIcon.COG));
        Label sairUsuario = new Label("Sair", GlyphsDude.createIcon(FontAwesomeIcon.SIGN_OUT));

        meusDadosUsuario.onMouseClickedProperty().set((event) -> {
            try {
                this.setContentFrame(new FrameEditarMeusDadosView(new FrameEditarMeusDadosVM(), this));
                this.tituloFrameLbl.setText("Meus dados");
                this.iconeTituloFrame.setGlyphName("COG");
                System.gc();
            } catch (IOException ex) {
                Logger.getLogger(PainelView.class.getName()).log(Level.SEVERE, null, ex);
            }
            popup.hide();
        });

        sairUsuario.onMouseClickedProperty().set((event) -> {
            System.out.println("Sair chamado");
            Session.getInstance().cleanSession();
            try {
                new LoginView(new LoginVM()).show();
                System.gc();
            } catch (IOException ex) {
                Logger.getLogger(PainelView.class.getName()).log(Level.SEVERE, null, ex);
            }
            popup.hide();
        });

        list.getItems().add(meusDadosUsuario);
        list.getItems().add(sairUsuario);

        System.out.println(super.getFXML());
        menuUsuarioBtn.setText(Session.getInstance().getPessoa().getNome().toUpperCase());
        menuUsuarioBtn.setOnMouseClicked(e -> {
            popup.show(menuUsuarioBtn, JFXPopup.PopupVPosition.TOP, JFXPopup.PopupHPosition.RIGHT);
        });

        initializePanelButtons();
    }

    protected JFXButton addButtonToMenuRoot(String tituloBtn, FontAwesomeIcon glyph, EventHandler<ActionEvent> event) {
        JFXButton b = new JFXButton(tituloBtn, GlyphsDude.createIcon(glyph));
        b.setGraphicTextGap(8);
        b.setOnAction(event);
        b.getStyleClass().add("titulo");

        b.setOnMouseClicked(value -> {
            this.tituloFrameLbl.setText(tituloBtn);
            this.iconeTituloFrame.setGlyphName(glyph.name());
            System.gc();
        });

        menuLateral.getChildren().add(b);
        return b;
    }

    protected JFXButton addButtonToMenuGroup(VBox parent, String tituloBtn, FontAwesomeIcon glyph, EventHandler<ActionEvent> event) {
        JFXButton b = new JFXButton(tituloBtn, GlyphsDude.createIcon(glyph));
        b.setGraphicTextGap(8);
        b.setOnAction(event);

        b.setOnMouseClicked(value -> {
            this.tituloFrameLbl.setText(tituloBtn);
            this.iconeTituloFrame.setGlyphName(glyph.name());
            System.gc();
        });

        b.getStyleClass().add("subtitulos");

        parent.getChildren().add(b);
        return b;
    }

    protected VBox createMenuGruop(String tituloLbl, FontAwesomeIcon glyph) {
        VBox v = new VBox();
        Label l = new Label(tituloLbl, GlyphsDude.createIcon(glyph));
        l.setGraphicTextGap(8);
        VBox.setMargin(l, new Insets(10, 0, 0, 0));
        v.getChildren().addAll(l);
        menuLateral.getChildren().add(v);
        return v;
    }

    protected void removeFromMenuRoot(Node v) {
        menuLateral.getChildren().remove(v);
    }

    protected void removeButtonFromMenuGroup(VBox parent, JFXButton btn) {
        parent.getChildren().remove(btn);
    }

    public void setContentFrame(Frame f) throws IOException {
        System.out.println("antes");

        Node n = f.load();
        System.out.println("depois");
        frame.getChildren().clear();
        frame.getChildren().add(n);
    }

    protected abstract void initializePanelButtons();

}
