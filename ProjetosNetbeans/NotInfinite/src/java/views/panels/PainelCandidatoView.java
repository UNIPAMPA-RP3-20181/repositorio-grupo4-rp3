/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.panels;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.scene.layout.VBox;
import viewmodel.ViewModel;
import viewmodel.panels.PainelCandidatoVM;

import java.io.IOException;

/**
 *
 * @author Michael Martins
 */

public class PainelCandidatoView extends PainelView {

    public PainelCandidatoView(ViewModel viewModel) {
        super(viewModel);
    }

    @Override
    protected void initializePanelButtons() {

        VBox mg1 = createMenuGruop("Solicitacoes", FontAwesomeIcon.FILE_TEXT);

        addButtonToMenuGroup(mg1,"Solicitar Seguro", FontAwesomeIcon.PLUS_CIRCLE, event -> {
            try {
                ((PainelCandidatoVM) myViewModel).showFrameSolicitarSeguro();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        addButtonToMenuGroup(mg1, "Visualizar", FontAwesomeIcon.EYE, event -> {
            try {
                ((PainelCandidatoVM) myViewModel).showFrameListaSolicitacoes();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });




        addButtonToMenuRoot("Contratar Seguro", FontAwesomeIcon.USER, event -> {
            try {
                ((PainelCandidatoVM) myViewModel).showFrameContratarSeguro();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
