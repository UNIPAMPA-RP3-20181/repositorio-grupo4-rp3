package views;

import viewmodel.ViewModel;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.StackPane;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public abstract class View implements Initializable {

    private FXMLLoader myFXML;
    protected ViewModel myViewModel;
    
    
    public static StackPane rootView;

    public View(ViewModel viewModel) {
        this.setFXML();
        this.myViewModel = viewModel;
        this.myViewModel.setView(this);
    }

    public static void setRootView(StackPane rootView) {
        View.rootView = rootView;
    }

    public void show() throws IOException{
        rootView.getChildren().clear();
        rootView.getChildren().add(this.load());
    }
    
    /*public void setWindow(Node node){
        rootView.getChildren().clear();
        rootView.getChildren().add(node);
    }*/

    public Node load() throws IOException {
        System.out.println(getFXML());
        return this.myFXML.load();
    }

    private void setFXML() {
        // Isso simplesmente pega o nome de classe sem o pacote
        this.setFXML(getClass().getName().split("\\.")[getClass().getName().split("\\.").length - 1].replaceAll("View", ""));
    }

    public void setFXML(String fxml) {
        this.myFXML = new FXMLLoader(View.class.getResource("/fxml/" + fxml + ".fxml"));
        this.myFXML.setController(this);
    }
    
    public String getFXML(){
        return (getClass().getName().split("\\.")[getClass().getName().split("\\.").length - 1].replaceAll("View", ""));
    }

    public StackPane getRoot() {
        return this.myFXML.getRoot();
    }
}
