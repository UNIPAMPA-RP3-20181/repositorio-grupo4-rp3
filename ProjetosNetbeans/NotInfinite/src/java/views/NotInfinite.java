/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import com.jfoenix.controls.JFXDecorator;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import viewmodel.LoginVM;
import viewmodel.ViewModel;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class NotInfinite extends Application {

    @Override
    public void start(Stage stage) {
        Image logomarca = new Image(NotInfinite.class.getResource("/images/iconeLogo.png").toExternalForm());
        stage.setTitle("Not Infinite");
        stage.getIcons().add(logomarca);

        try {
            LoginView login = new LoginView(new LoginVM());
            JFXDecorator decorator;

            MainView mainView = new MainView(new ViewModel() {}, login.load());
            decorator = new JFXDecorator(stage, mainView.load());

            decorator.setCustomMaximize(true);
            decorator.setGraphic(GlyphsDude.createIcon(FontAwesomeIcon.GRATIPAY, "18px"));
            Scene scene = new Scene(decorator, 1024, 576);

            final ObservableList<String> stylesheets = scene.getStylesheets();
            stylesheets.addAll(
                    NotInfinite.class.getResource("/css/paletaCores.css").toExternalForm(),
                    NotInfinite.class.getResource("/css/JFoenix.css").toExternalForm()
            );

            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(NotInfinite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
