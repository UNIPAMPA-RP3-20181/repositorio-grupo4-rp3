/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXTextField;
import exceptions.EmptyFieldException;
import exceptions.IncorrectPasswordException;
import exceptions.IncorrectUsernameException;
import exceptions.NullFieldException;

import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import viewmodel.ViewModel;
import viewmodel.LoginVM;
//import views.Alerta;
//import views.ViewFXML;

/**
 * FMXL ViewModel LoginView
 *
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class LoginView extends View {

    @FXML
    private JFXTextField email;

    @FXML
    private JFXPasswordField senha;

    @FXML
    private JFXButton btnCadastro;

    public LoginView(ViewModel viewModel) {
        super(viewModel);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        email.requestFocus();
    }

    @FXML
    private void entrar() throws IOException {
        try {
            ((LoginVM) myViewModel).autenticar();
        } catch (SQLException ex) {
            new Alerta().exibir("Conexão", "Erro ao se conectar com o servidor, verifique a conexão.");
        } catch (NullFieldException | EmptyFieldException ex) {
            new Alerta().exibir("Erro ao entrar", "Os campos de usuário de senha não podem estar vazios!");
        } catch (IncorrectUsernameException | IncorrectPasswordException ex) {
            new Alerta().exibir("Erro ao entrar", "Usuário e/ou senha estão incorretos.");
        }
        //  ((LoginVM) myViewModel).autenticarComoDesenvolvedor(email.getText().toLowerCase());

    }

    @FXML
    private void relatarMorte() throws IOException {
        ((LoginVM) myViewModel).telaRelatarMorte();
    }

    @FXML
    private void eventEnter(KeyEvent event) throws IOException {
        if (event.getCode() == KeyCode.ENTER) {
            this.entrar();
        }
    }

    @FXML
    void cadastro(ActionEvent event) throws IOException {
        ((LoginVM) myViewModel).telaCadastro();
    }

    public String getUsername() {
        return email.getText();
    }

    public String getPassword() {
        return senha.getText();
    }
}
