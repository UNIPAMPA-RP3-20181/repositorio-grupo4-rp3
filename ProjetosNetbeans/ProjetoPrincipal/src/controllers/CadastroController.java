/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXTextField;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import models.TextFieldFormatter;
import models.Usuario;
import models.dao.CadastroDAO;
import views.Alerta;

/**
 * FMXL Controller Login
 *
 * @author marti
 */
public class CadastroController extends Controller {

    @FXML
    private TextField usuario;

    @FXML
    private TextField email;

    @FXML
    private PasswordField senha;

    @FXML
    private PasswordField confirmarSenha;

    @FXML
    private JFXButton btnRegistrar;

    @FXML
    private JFXTextField nomeCompleto;

    @FXML
    private JFXTextField CPF;

    @FXML
    private JFXComboBox<String> sexo;

    @FXML
    private JFXDatePicker nascimento;

    @FXML
    private JFXComboBox<String> estadoCivil;

    @FXML
    private JFXTextField ocupacao;

    @FXML
    private JFXTextField endereco;

    @FXML
    private JFXTextField bairro;

    @FXML
    private JFXTextField cidade;

    @FXML
    private JFXTextField CEP;

    @FXML
    private JFXTextField nroTelFIXO;

    @FXML
    private JFXTextField nroTelCEL;

    ObservableList<String> sexoList = FXCollections.observableArrayList("F", "M");

    ObservableList<String> estadoCivilList = FXCollections.observableArrayList(
            "Solteiro(a)", "Casado(a)", "Separado(a)", "Divorciado(a)", "Viúvo(a)");

    public CadastroController() throws IOException {
        super("/views/Cadastro.fxml");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        sexo.setItems(sexoList);
        estadoCivil.setItems(estadoCivilList);
    }

    @FXML
    void registrar(ActionEvent event) throws IOException {
        if (validarCampos()) {
            Usuario user = new Usuario(usuario.getText(), email.getText(), senha.getText());
            CadastroDAO dao = new CadastroDAO();
            try {
                dao.create(user);
            } catch (SQLException ex) {
                Logger.getLogger(CadastroController.class.getName()).log(Level.SEVERE, null, ex);
            }
            super.setWindow(new LoginController());
        }
    }

    @FXML
    void voltar(ActionEvent event) throws IOException {
        Alerta.addBtn("Sim", acao -> {
            try {
                super.setWindow(new LoginController());
            } catch (IOException ex) {
                Logger.getLogger(ContratarSeguroController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        Alerta.addBtn("Não", acao -> {
            System.out.println("Ficou no sistema");
        });
        Alerta.mostrar("Volta!", "Sair da tela de cadastro?", true);

    }

    @FXML
    void maskCEP(KeyEvent event) {
        TextFieldFormatter mask = new TextFieldFormatter();
        mask.setMask("#####-###");
        mask.setCaracteresValidos("0123456789");
        mask.setTf(CEP);
        mask.formatter();
    }

    @FXML
    void maskCPF(KeyEvent event) {
        TextFieldFormatter mask = new TextFieldFormatter();
        mask.setMask("###.###.###-##");
        mask.setCaracteresValidos("0123456789");
        mask.setTf(CPF);
        mask.formatter();
    }

    @FXML
    void maskTELCELL(KeyEvent event) {
        TextFieldFormatter mask = new TextFieldFormatter();
        mask.setMask("(##)#####-####");
        mask.setCaracteresValidos("0123456789");
        mask.setTf(nroTelCEL);
        mask.formatter();
    }

    @FXML
    void maskTELFIXO(KeyEvent event) {
        TextFieldFormatter mask = new TextFieldFormatter();
        mask.setMask("(##)####-####");
        mask.setCaracteresValidos("0123456789");
        mask.setTf(nroTelFIXO);
        mask.formatter();
    }

    private boolean validarCampos() {
        if (nomeCompleto.getText().isEmpty() || CPF.getText().isEmpty() || sexo.getEditor().getText().isEmpty()
                || nascimento.getEditor().getText().isEmpty() || ocupacao.getText().isEmpty() || endereco.getText().isEmpty()
                || bairro.getText().isEmpty() || cidade.getText().isEmpty() || CEP.getText().isEmpty()
                || nroTelCEL.getText().isEmpty()) {
            Alerta.addBtn("Ok", acao -> {
            });
            Alerta.mostrar("ATENÇÃO!", "Preencha todos os campos pessoais.", true);
            return false;
        }
        return true;
    }
}
