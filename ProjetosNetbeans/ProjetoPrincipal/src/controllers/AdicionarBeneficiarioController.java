/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import models.dao.BeneficiarioDao;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.scene.control.ToggleGroup;
import models.Beneficiario;
import models.Parentesco;

/**
 * FXML Controller class
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class AdicionarBeneficiarioController extends Controller {

    @FXML
    private JFXTextField cpf;

    @FXML
    private JFXTextField nomeCompleto;

    @FXML
    private JFXRadioButton sexoF;

    @FXML
    private JFXRadioButton sexoM;

    @FXML
    private JFXTextField email;

    @FXML
    private JFXDatePicker dataNascimento;

    @FXML
    private JFXTextField telefone;

    @FXML
    private JFXTextField cep;

    @FXML
    private JFXTextField endereco;

    @FXML
    private JFXTextField bairro;

    @FXML
    private JFXTextField cidade;

    @FXML
    private JFXComboBox<String> parentesco;
    
    private ToggleGroup group;

    public AdicionarBeneficiarioController() throws IOException {
        super("/views/AdicionarBeneficiario.fxml");
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        for (Parentesco parentescoEnum : Parentesco.values()) {
            this.parentesco.getItems().add(parentescoEnum.getNome());
        }

        this.group = new ToggleGroup();
        sexoM.setToggleGroup(group);
        sexoF.setToggleGroup(group);
    }
    
    @FXML
    private void cadastrar(){
        
        String formNome = nomeCompleto.getText();
        String formCPF = cpf.getText();
        
        char sexo = ' ';
        
        if(group.getSelectedToggle().isSelected()){
            if(sexoM.isSelected()){
                sexo = 'M';     
            } else {
                sexo = 'F';
            }
        } else {
            System.out.println("Selecione um sexo");
        }
        
        String formEmail = email.getText();
        Date formDataNascimento = Date.from(dataNascimento.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
        String formTelefone = telefone.getText();
        String formCEP = cep.getText();
        String formEndereco = endereco.getText();
        String formBairro = bairro.getText();
        String formCidade = cidade.getText();
        String formParentesco = parentesco.getValue();
        
        Beneficiario beneficiario = new Beneficiario(Parentesco.getParentesco(formParentesco), formNome, formCPF, formEmail, formTelefone, formDataNascimento, sexo, formCEP, formCidade, formEndereco, formBairro);
        BeneficiarioDao beneficiarioDao = new BeneficiarioDao();
        try {
            beneficiarioDao.insert(beneficiario);
        } catch (SQLException ex) {
            Logger.getLogger(AdicionarBeneficiarioController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalStateException ex) {
            Logger.getLogger(AdicionarBeneficiarioController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AdicionarBeneficiarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
