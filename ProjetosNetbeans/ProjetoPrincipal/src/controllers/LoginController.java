/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Date;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import controllers.avaliarsolicitacao.ListaSolicitacoesAvaliarController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import models.Crypt;
import models.Session;
import models.dao.LoginMaker;
import views.Alerta;

/**
 * FMXL Controller Login
 *
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class LoginController extends Controller {

    @FXML
    JFXTextField email;
    @FXML
    JFXPasswordField senha;
    @FXML
    JFXToggleButton toogle;
    @FXML
    private JFXButton btnCadastro;
    
    public LoginController() throws IOException {
        super("/views/Login.fxml");
        Session.getInstance().cleanSession();

    }

    public LoginController(boolean deleteCache) throws IOException {
        super("/views/Login.fxml");
        if (deleteCache) {
            Session.getInstance().cleanSessionAndDeleteCache();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        email.requestFocus();
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
            @Override                        // your code here

            public void run() {
                if (Session.getInstance().verifyIfExistArchivedSession()) {
                    System.out.println("entrando");
                    try {
                        redirectToScreen();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        },
                100
        );

    }

    @FXML
    private void entrar() throws IOException {

//        // ====== OFICIAL ======
//        MessageDigest md3 = null;
//
//        String salt = "%Ü%$JYRG$T%|%RJY%|I&|(*&TWYR$$%$@$$ŸGRDRUU&$%*()(&%$R%T&G";
//        String pass = Crypt.sha512((email.getText() + senha.getText()), salt);
//        
//        
//        
//        System.out.println("Generated pass:" + pass);
//
//        tryToLogin(email.getText(), pass);
        // ====== PARA FINS DE DESENVOLVIMENTO ======
//        super.setWindow(new ContratarSeguroController());
//        if (email.getText().equals("candidato")) {
//            super.setWindow(new PainelController());
//        }
//        if (email.getText().equals("corretor")) {
//            super.setWindow(new ListaSolicitacoesAvaliarController());
//        }

        switch (email.getText().toLowerCase()) {

            case "candidato":
                super.setWindow(new MenuTesteController());
                break;
            case "segurado":
            case "s":
                super.setWindow(new PainelController());
                break;
            case "corretor":
                super.setWindow(new ListaSolicitacoesAvaliarController());
                break;
            default:
                super.setWindow(new PainelController());
//                showEmails();
        }

    }

    private void tryToLogin(String user, String senha) throws IOException {
        LoginMaker lm = new LoginMaker();
        Session session = Session.getInstance();
        if (lm.tryToLogin(user, senha, session, toogle.isSelected())) {
            redirectToScreen();

        } else {
            Alerta.mostrar("Erro ao fazer login", "Usuário ou Senha incorretos", true);
        }
    }

    private void redirectToScreen() throws IOException {
        switch (Session.getInstance().getTipoPessoa()) {
            case "candidato":
                System.out.println("candidato");
                super.setWindow(new MenuTesteController());
                System.out.println("devia ter entrado");
                break;
            case "corretor":
                System.out.println("corretor");
                super.setWindow(new ListaSolicitacoesAvaliarController());
                System.out.println("devia ter entrado");

                break;
            case "avaliador":
                System.out.println("avaliador");
                Alerta.mostrar("AINDA NÃO", "Não foi implementada a tela pros avaliadores", true);
                System.out.println("devia ter entrado");
                break;

            case "segurado":
            case "s":
                System.out.println("segurado");
                super.setWindow(new PainelController());
                System.out.println("devia ter entrado");
                break;
            default:
                System.out.println("nenhum");
        }
    }

    @FXML
    private void relatarMorte(ActionEvent event) throws IOException {
        super.setWindow(new RelatarMorteController());
    }

    @FXML
    private void showEmails() {
        Alerta.mostrar("Usuários para teste",
                "Para entrar como corretor: \"corretor\"\n"
                + "Para entrar como candidato: \"candidato\"\n"
                + "Para entrar como segurado: \"segurado\"", true);
        Date d = new Date();
    }

    @FXML
    private void eventEnter(KeyEvent event) throws IOException {
        if (event.getCode() == KeyCode.ENTER) {
            this.entrar();
        }

    }
    
    @FXML
    void cadastro(ActionEvent event) throws IOException {
        super.setWindow(new CadastroController());
    }
}
