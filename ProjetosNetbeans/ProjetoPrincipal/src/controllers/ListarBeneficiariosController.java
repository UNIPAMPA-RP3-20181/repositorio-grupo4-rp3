/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import models.Beneficiario;
import models.dao.BeneficiarioDao;

/**
 * FXML Controller class
 *
 * @author sathelerdev
 */
public class ListarBeneficiariosController extends Controller {

    @FXML
    private TableView<Beneficiario> tabelaBeneficiarios;

    public ListarBeneficiariosController() throws IOException {
        super("/views/ListarBeneficiarios.fxml");
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tabelaBeneficiarios.setEditable(false);
        (tabelaBeneficiarios.getColumns().get(0)).setCellValueFactory(new PropertyValueFactory("nome"));

        List<Beneficiario> beneficiarios = new ArrayList();
        BeneficiarioDao beneficiariosDAO = new BeneficiarioDao();
        try {
            beneficiarios = beneficiariosDAO.select();
        } catch (SQLException ex) {
            Logger.getLogger(ListarBeneficiariosController.class.getName()).log(Level.SEVERE, null, ex);
        }

        final ObservableList<Beneficiario> data = FXCollections.observableArrayList(beneficiarios);

        tabelaBeneficiarios.setItems(data);
    }

    @FXML
    void acaoAdicionar(ActionEvent event) {
        
    }

    @FXML
    void acaoEditar(ActionEvent event) {

    }

    @FXML
    void acaoRemover(ActionEvent event) {

    }

}
