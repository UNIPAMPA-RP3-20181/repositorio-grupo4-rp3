/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import controllers.MainController;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class Main extends Application {

    public static MainController mainController;

    @Override
    public void start(Stage stage) throws Exception {
        mainController = new MainController(stage, "NotInfinite", false);
        mainController.initStage();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
