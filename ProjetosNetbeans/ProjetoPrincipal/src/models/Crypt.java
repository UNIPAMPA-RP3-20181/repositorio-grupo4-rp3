package models;

import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class Crypt {
    public static String sha512(String input, String salt) {
        String sha1 = null;
        try {
            MessageDigest msdDigest = MessageDigest.getInstance("SHA-512");
            System.out.println("buffer: "+input.length());
            msdDigest.update(salt.getBytes("UTF-8"), 0, 18);
            sha1 = DatatypeConverter.printHexBinary(msdDigest.digest(input.getBytes("UTF-8")));
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException ignored) {
        }
        return sha1;
    }


}
