package models;

import com.mysql.jdbc.StringUtils;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

public class SessionToBeSaved implements Serializable {
    private String savedToken;
    private int id;

    SessionToBeSaved(String savedToken, int id) throws UnsupportedEncodingException {
        this.savedToken = obfuscate(savedToken);
        this.id = id;
    }

    String getSavedToken() {
        return deobfuscate(savedToken);
    }

    int getSavedId() {
        return id;

    }

    private static String obfuscate(String s)
    {
        StringBuilder buf = new StringBuilder();
        byte[] b = s.getBytes(StandardCharsets.UTF_8);

        buf.append("OBF:");
        for (int i = 0; i < b.length; i++)
        {
            byte b1 = b[i];
            byte b2 = b[b.length - (i + 1)];
            if (b1<0 || b2<0)
            {
                int i0 = (0xff&b1)*256 + (0xff&b2);
                String x = Integer.toString(i0, 36).toLowerCase();
                buf.append("U0000",0,5-x.length());
                buf.append(x);
            }
            else
            {
                int i1 = 127 + b1 + b2;
                int i2 = 127 + b1 - b2;
                int i0 = i1 * 256 + i2;
                String x = Integer.toString(i0, 36).toLowerCase();

                int j0 = Integer.parseInt(x, 36);
                int j1 = (i0 / 256);
                int j2 = (i0 % 256);
                byte bx = (byte) ((j1 + j2 - 254) / 2);

                buf.append("000",0,4-x.length());
                buf.append(x);
            }

        }
        return buf.toString();

    }

    /* ------------------------------------------------------------ */
    public static String deobfuscate(String s)
    {
        if (s.startsWith("OBF:")) s = s.substring(4);

        byte[] b = new byte[s.length() / 2];
        int l = 0;
        for (int i = 0; i < s.length(); i += 4)
        {
            if (s.charAt(i)=='U')
            {
                i++;
                String x = s.substring(i, i + 4);
                int i0 = Integer.parseInt(x, 36);
                byte bx = (byte)(i0>>8);
                b[l++] = bx;
            }
            else
            {
                String x = s.substring(i, i + 4);
                int i0 = Integer.parseInt(x, 36);
                int i1 = (i0 / 256);
                int i2 = (i0 % 256);
                byte bx = (byte) ((i1 + i2 - 254) / 2);
                b[l++] = bx;
            }
        }

        return new String(b, 0, l,StandardCharsets.UTF_8);
    }
/*
    private transient String source = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private transient String target = "Q5A8ZWS0XEDC6RFVT9GBY4HNU3J2MI1KO7LP";

    private String obfuscate(String s) {
        char[] result = new char[s.length()];
        for (int i = 0; i < s.length() - 1; i++) {
            char c = s.charAt(i);
            int index = source.indexOf(c);
            result[i] = target.charAt(index);
        }

        return new String(result);
    }

    private String unobfuscate(String s) {
        char[] result = new char[s.length()];
        for (int i = 0; i < s.length() - 1; i++) {
            char c = s.charAt(i);
            System.out.println(c);
            int index = target.indexOf(c);
            result[i] = source.charAt(index);
        }
        System.out.println(result);
        return new String(result);

    }
*/
}
