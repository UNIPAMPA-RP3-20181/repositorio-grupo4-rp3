/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import models.*;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class CorretorDao extends DAO {

    public CorretorDao() {
        super();
    }

    @Override
    public void delete(Model model) {

    }

    @Override
    public void update(Model model) {

    }



    @Override
    public void insert(Model model) throws SQLException, IllegalStateException, IOException {
    }

    private Candidato converter(Model model) {
        Candidato modelToConverter = null;

        if (model instanceof Candidato) {
            modelToConverter = (Candidato) model;
        } else {
            throw new ClassCastException("A model não é uma solicitação de seguro");
        }

        return modelToConverter;
    }

    @Override
    public List<Model> select() throws SQLException {
        conexao.abrirConexao();
        List<Model> retorno = new ArrayList<>();
        PreparedStatement stmt = conexao.prepararDeclaracao("select pessoas.id, nome, cpf, email, telefone, " +
                "datanascimento, sexo, cep, endereco, cidade, bairro, datacontratacao, ativo " +
                "from funcionarios, pessoas, tipofuncionario, tipopessoa " +
                "where tipofuncionario.id=funcionarios.tipofuncionario " +
                "and pessoas.tipopessoa=`tipopessoa`.`id` " +
                "and tipopessoa.descricao=? " +
                "and tipofuncionario.descricao=? ");

        stmt.setString(1, "Funcionario");
        stmt.setString(2, "Corretor");

        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            //id, nome, cpf, email, telefone, datanascimento, sexo, cep
            Corretor c = new Corretor(
                    rs.getString("nome"),
                    rs.getString("cpf"), //cpf
                    rs.getString("email"),
                    rs.getString("telefone"),
                    rs.getDate("datanascimento"), //nasc
                    rs.getString("sexo").toUpperCase().charAt(0), //sexo
                    rs.getString("cep"), //cep
                    rs.getString("cidade"),//cidadef
                    rs.getString("endereco"), //endereco
                    rs.getString("bairro"),//bairro
                    rs.getDate("datacontratacao"),
                    rs.getBoolean("ativo"));//bairro
            c.setId(rs.getInt("id"));


            retorno.add(c);
        }
        return retorno;
    }


}
