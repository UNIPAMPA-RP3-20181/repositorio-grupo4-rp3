package models.dao;

import bancoDeDados.Conexao;
import exceptions.NotFoundOnDBException;
import models.Session;
import models.factories.AvaliadorFactory;
import models.factories.CandidatoFactory;
import models.factories.CorretorFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginMaker {
    protected Conexao conexao;

    public LoginMaker() {
        this.conexao = Conexao.getInstance();
        conexao.init("localhost", 3306, "segurodevida2", "root", "carambolatech");
    }

    public boolean tryToLogin(String user, String senha, Session session, boolean saveSession) {
        if (senha == null || user == null || senha.isEmpty() || user.isEmpty())
            return false;
        try {
            conexao.abrirConexao();
            PreparedStatement stmt = conexao.prepararDeclaracao("select usuarios.idpessoa as id, tipopessoa.descricao as tipopessoa " +
                    "from usuarios, pessoas, tipopessoa " +
                    "where usuarios.idpessoa=pessoas.id " +
                    "and pessoas.tipopessoa=tipopessoa.id " +
                    "and usuarios.username=? and usuarios.password=? limit 1");
            stmt.setString(1, user);
            stmt.setString(2, senha);
            ResultSet rs = stmt.executeQuery();

            if (!rs.first())
                return false;

            int id = rs.getInt("id");
            String tipopessoa = rs.getString("tipopessoa");

            boolean retorno =  session.makePessoaSession(id, tipopessoa);
            if (saveSession) {
                System.out.println("trying to save");
                session.saveSession();
            }
            return retorno;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

}


