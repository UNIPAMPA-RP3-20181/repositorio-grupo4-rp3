package models.factories;

import exceptions.NotFoundOnDBException;
import models.Corretor;
import models.Model;
import models.dao.CorretorDao;

import java.sql.SQLException;
import java.util.List;

public class CorretorFactory {
   static public Corretor getCorretorById(int id) throws NotFoundOnDBException {
       CorretorDao cd = new CorretorDao();
       try {
           List<Model> ml = cd.select();
           for (Model m : ml) {
               if (m instanceof Corretor) {
                   if (((Corretor) m).getId() == id) {
                       return (Corretor) m;
                   }
               }
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }
       throw new NotFoundOnDBException("Não foi possível encontrar o Corretor pelo ID informado");

   }
}
