package models.factories;

import exceptions.NotFoundOnDBException;
import models.Avaliador;
import models.Model;
import models.dao.AvaliadorDao;
import models.dao.CorretorDao;

import java.sql.SQLException;

public class AvaliadorFactory {
   static public Avaliador getAvaliadorById(int id) throws NotFoundOnDBException {
       AvaliadorDao cd = new AvaliadorDao();
       try {
           for (Model m : cd.select()) {
               if (m instanceof Avaliador) {
                   if (((Avaliador) m).getId() == id) {
                       return (Avaliador) m;
                   }
               }
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }
       throw new NotFoundOnDBException("Não foi possível encontrar o Avaliador pelo ID informado");

   }
}
