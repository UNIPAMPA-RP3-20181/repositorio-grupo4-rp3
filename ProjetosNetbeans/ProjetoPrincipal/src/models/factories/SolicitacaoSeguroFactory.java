package models.factories;

import exceptions.NotFoundOnDBException;
import models.Model;
import models.SolicitacaoSeguro;
import models.dao.SolicitacaoSeguroDao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SolicitacaoSeguroFactory {

    static public Model getSolicitacaoByNumeroSolicitacao(int nSol) throws NotFoundOnDBException {
        try {
            List<Model> lm = new SolicitacaoSeguroDao().select();
            for (Model m : lm) {
                if (m instanceof SolicitacaoSeguro) {
                    if (((SolicitacaoSeguro) m).getNumeroSolicitacao() == nSol) {
                        return m;
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        throw new NotFoundOnDBException("Não foi possível encontrar a solicitação pelo ID informado");
    }

    static public List<SolicitacaoSeguro> getTodasSolicitacoesComDataVisitaNaoMarcadasENaoAvaliadas() {
        List<SolicitacaoSeguro> lss = new ArrayList<>();
        List<Model> old = null;
        try {
            old = new SolicitacaoSeguroDao().select();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        assert old != null;
        for (Model m : old) {
            if (m instanceof SolicitacaoSeguro) {
                SolicitacaoSeguro s = (SolicitacaoSeguro) m;
                if (s.getDataVisitaCandidato() == null && !s.isAprovadaSolicitacao() && s.getMotivoReprovacao()==null)
                    lss.add(s);
            }

        }
        return lss;
    }

    static public List<SolicitacaoSeguro> getAll() {
        List<SolicitacaoSeguro> lss = new ArrayList<>();
        List<Model> old = null;
        try {
            old = new SolicitacaoSeguroDao().select();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        assert old != null;
        for (Model m : old) {
            if (m instanceof SolicitacaoSeguro) {
                lss.add((SolicitacaoSeguro) m);

            }

        }
        return lss;
    }

    static public void atualizarSolicitacaoDeSeguro(SolicitacaoSeguro s) {
        try {
            new SolicitacaoSeguroDao().update(s);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
